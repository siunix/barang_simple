<?php
class Barang_engine extends Db_engine
{
    private $table_name;

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "barang";
    }

    function barang_get_list($filters = array(), $pagination = false, $datatables = false)
    {
        $column_search = array(null, "created", "code", "name", "jumlah", "perusahaan_name");
        $column_order = $column_search;
        $order = array("created" => "DESC");

        $this->db->select("$this->table_name.*, perusahaan.name AS perusahaan_name");
        $this->db->from("$this->table_name");
        $this->db->join("perusahaan", "perusahaan.id = $this->table_name.perusahaan_id");

        // $this->db->select("*");
        // $this->db->from($this->table_name);

        if ($datatables) {
            $this->generate_datatables_input($filters, $column_search, $column_order, $order, $pagination);
        } else {
            foreach ($filters as $key => $value) {
                switch ($key) {
                    case 'id':
                        $this->db->where("$this->table_name.id", (int)$value);
                        break;
                    case 'hash_id':
                        $this->db->where("md5($this->table_name.id)", trim($value));
                        break;
                    case 'name':
                        $this->db->where("$this->table_name.name", trim($value));
                        break;
                    case 'code':
                        $this->db->where("$this->table_name.code", trim($value));
                        break;
                    case 'jumlah':
                        $this->db->where("$this->table_name.jumlah", (int)$value);
                        break;
                    default:
                        break;
                }
            }
        }

        $res = $this->db->get()->result_array();
        return $res;
    }

    function barang_save($data = array())
    {
        $id = (int) $data["id"];
        $created = $data["created"];
        $creator_user_id = (int) $data["creator_user_id"];
        $creator_user_name = trim($data["creator_user_name"]);
        $code = isset($data["code"]) ? trim($data["code"]) : "";
        $name = isset($data["name"]) ? trim($data["name"]) : "";
        $jumlah = isset($data["jumlah"]) ? trim($data["jumlah"]) : 0;
        $perusahaan_id = isset($data["perusahaan_id"]) ? trim($data["perusahaan_id"]) : 0;

        $this->db->set(array(
            "id" => $id,
            "created" => $created,
            "creator_user_id" => $creator_user_id,
            "creator_user_name" => $creator_user_name,
            "code" => $code,
            "name" => $name,
            "jumlah" => $jumlah,
            "perusahaan_id" => $perusahaan_id,
        ));

        $this->db->replace($this->table_name);

        if ($id == 0) return $this->db->insert_id();
        return $id;
    }

    function barang_delete($id = 0)
    {
        if ((int)$id == 0) return 0;

        $this->db->where("id", $id);
        $this->db->delete($this->table_name);

        return $id;
    }
    // ====================================================
    // ** END of OBJECT USER LEVEL
    // ====================================================
}
