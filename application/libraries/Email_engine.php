<?php
class Email_engine
{

    private $config;
    public function __construct()
    {
        $config = array();
        $config["protocol"] = "smtp";
        $config["smtp_host"] = "ssl://smtp.googlemail.com";
        $config["smtp_port"] = "465";
        $config["smtp_user"] = EMAIL_SMTP_USER;
        $config["smtp_pass"] = EMAIL_SMTP_PASS;
        $config["charset"] = "utf-8";
        $config["mailtype"] = "html";
        $config["newline"] = "\r\n";

        $this->config = $config;
    }

    function send_mail($data = array())
    {
        $email_target = isset($data["email"]) && !empty($data["email"]) ? trim($data["email"]) : "";
        $email_subject = isset($data["subject"]) && !empty($data["subject"]) ? trim($data["subject"]) : "";
        $email_message = isset($data["message"]) && !empty($data["message"]) ? trim($data["message"]) : "";

        if (empty($email_target)) return false;
        if (empty($email_subject)) return false;
        if (empty($email_message)) return false;

        $list = array($email_target);
        $ci = &get_instance();
        $ci->email->initialize($this->config);
        $ci->email->from(EMAIL_FROM, EMAIL_ALIAS);
        $ci->email->to($list);
        $ci->email->subject($email_subject);
        $ci->email->message($email_message);

        $res = $ci->email->send();
        return $res;
    }
}
