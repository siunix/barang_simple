<?php

class Area_engine extends Db_engine {
    //put your code here

    public function __construct() {
        parent::__construct();
    }

    // ===========================================
    // OBJECT ADM PROVINCE
    // ===========================================
    function province_get_list($filters = array()){
        foreach ($filters as $key => $value) {
            switch ($key) {
                case 'id':
                    $this->db->where("id", (int) $value);
                    break;
                case 'hash_id':
                    $this->db->where("md5(id)", trim($value));
                    break;
                default:
                    break;
            }
        }

        $this->db->select("*");
        $this->db->from("provinces");
        $this->db->order_by("name");
        $res = $this->db->get()->result_array();

        return $res;
    }
    // ===========================================
    // end of OBJECT ADM PROVINCE
    // ===========================================

    // ===========================================
    // OBJECT ADM REGENCIES
    // ===========================================
    function regency_get_list($filters = array()){
        foreach ($filters as $key => $value) {
            switch ($key) {
                case 'id':
                    $this->db->where("id", (int) $value);
                    break;
                case 'hash_id':
                    $this->db->where("md5(id)", trim($value));
                    break;
                case 'province_id':
                    $this->db->where("province_id", (int) $value);
                    break;
                case 'province_hash_id':
                    $this->db->where("md5(province_id)", trim($value));
                    break;
                default:
                    break;
            }
        }

        $this->db->from("regencies");
        $this->db->order_by("name");
        $res = $this->db->get()->result_array();

        return $res;
    }
    // ===========================================
    // end of OBJECT ADM REGENCIES
    // ===========================================

    // ===========================================
    // OBJECT ADM DISTRICT
    // ===========================================
    function district_get_list($filters = array()){
        foreach ($filters as $key => $value) {
            switch ($key) {
                case 'id':
                    $this->db->where("id", (int) $value);
                    break;
                case 'hash_id':
                    $this->db->where("md5(id)", trim($value));
                    break;
                case 'regency_id':
                    $this->db->where("regency_id", (int) $value);
                    break;
                case 'regency_hash_id':
                    $this->db->where("md5(regency_id)", trim($value));
                    break;
                default:
                    break;
            }
        }

        $this->db->from("districts");
        $this->db->order_by("name");
        $res = $this->db->get()->result_array();

        return $res;
    }
    // ===========================================
    // end of OBJECT ADM DISTRICT
    // ===========================================

    // ===========================================
    // OBJECT ADM VILLAGE
    // ===========================================
    function village_get_list($filters = array()){
        foreach ($filters as $key => $value) {
            switch ($key) {
                case 'id':
                    $this->db->where("id", (int) $value);
                    break;
                case 'hash_id':
                    $this->db->where("md5(id)", trim($value));
                    break;
                case 'district_id':
                    $this->db->where("district_id", (int) $value);
                    break;
                case 'district_hash_id':
                    $this->db->where("md5(district_id)", trim($value));
                    break;
                default:
                    break;
            }
        }

        $this->db->from("villages");
        $this->db->order_by("name");
        $res = $this->db->get()->result_array();

        return $res;
    }
    // ===========================================
    // end of OBJECT ADM VILLAGE
    // ===========================================
}
