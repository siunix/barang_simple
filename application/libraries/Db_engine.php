<?php

class Db_engine
{

    protected $db;

    public function __construct()
    {
        $ci = &get_instance();
        $this->db = $ci->db;
    }

    public function generate_datatables_input($filters = array(), $column_search = array(), $column_order = array(), $order = array(), $pagination = false)
    {

        if (isset($filters["search"]["value"]) && !empty($filters["search"]["value"])) {
            // remove null for global search
            $tmp = $column_search;
            $column_search = array();
            foreach ($tmp as $i => $v) {
                if ($v == null || empty($v)) continue;
                $column_search[] = $v;
            }

            $i = 0;
            foreach ($column_search as $item) {
                if ($i == 0) {
                    $this->db->group_start();
                    $this->db->like($item, $filters["search"]["value"]);
                } else {
                    $this->db->or_like($item, $filters["search"]["value"]);
                }

                if (count($column_search) - 1 == $i) {
                    $this->db->group_end();
                }
                $i++;
            }
        }

        $i = 0;
        foreach ($column_search as $index => $item) {
            if (isset($filters['columns'][$index])) {
                if (!empty($filters["columns"][$index]['search']['value'])) {
                    $value = $filters["columns"][$index]['search']['value'];
                    $this->db->like($item, trim($value));
                }
            }

            $i++;
        }

        if (isset($filters["order"])) {
            if (isset($column_order[$filters["order"][0]["column"]])) {
                $this->db->order_by($column_order[$filters["order"][0]["column"]], $filters["order"][0]["dir"]);
            }
        } else if (isset($order)) {
            $this->db->order_by(key($order), $order[key($order)]);
        }

        if ($pagination) {
            $start  = isset($filters["start"]) ? (int) $filters["start"] : 0;
            $length = isset($filters["length"]) ? (int) $filters["length"] : -1;

            if ($length != -1) $this->db->limit($length, $start);
        }
    }
}
