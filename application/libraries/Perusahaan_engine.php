<?php
class Perusahaan_engine extends Db_engine
{
    private $table_name;

    public function __construct()
    {
        parent::__construct();
        $this->table_name = "perusahaan";
    }

    function perusahaan_get_list($filters = array(), $pagination = false, $datatables = false)
    {
        $column_search = array(null, "name", "province_name", "regency_name", "district_name", "village_name", "is_active");
        $column_order = $column_search;
        $order = array("name" => "DESC");

        $this->db->select("*");
        $this->db->from($this->table_name);

        if ($datatables) {
            $this->generate_datatables_input($filters, $column_search, $column_order, $order, $pagination);
        } else {
            foreach ($filters as $key => $value) {
                switch ($key) {
                    case 'id':
                        $this->db->where("id", (int)$value);
                        break;
                    case 'hash_id':
                        $this->db->where("md5(id)", trim($value));
                        break;
                    case 'name':
                        $this->db->where("name", trim($value));
                        break;
                    case 'province_id':
                        $this->db->where("province_id", (int)$value);
                        break;
                    case 'regency_id':
                        $this->db->where("regency_id", (int)$value);
                        break;
                    case 'district_id':
                        $this->db->where("district_id", (int)$value);
                        break;
                    case 'village_id':
                        $this->db->where("village_id", (int)$value);
                        break;
                    case 'is_active':
                        $this->db->where("is_active", (int)$value);
                        break;
                    default:
                        break;
                }
            }
        }

        $res = $this->db->get()->result_array();
        return $res;
    }

    function perusahaan_save($data = array())
    {
        $id = (int) $data["id"];
        $created = $data["created"];
        $creator_user_id = (int) $data["creator_user_id"];
        $creator_user_name = trim($data["creator_user_name"]);
        $name = isset($data["name"]) ? trim($data["name"]) : "";
        $alamat = isset($data["alamat"]) ? trim($data["alamat"]) : "";
        $province_id = isset($data["province_id"]) ? (int) $data["province_id"] : 0;
        $province_name = isset($data["province_name"]) ? trim($data["province_name"]) : "";
        $regency_id = isset($data["regency_id"]) ? (int) $data["regency_id"] : 0;
        $regency_name = isset($data["regency_name"]) ? trim($data["regency_name"]) : "";
        $district_id = isset($data["district_id"]) ? (int) $data["district_id"] : 0;
        $district_name = isset($data["district_name"]) ? trim($data["district_name"]) : "";
        $village_id = isset($data["village_id"]) ? (int) $data["village_id"] : 0;
        $village_name = isset($data["village_name"]) ? trim($data["village_name"]) : "";
        $is_active = isset($data["is_active"]) ? (int)$data["is_active"] : 0;
        $set_active_date = isset($data["set_active_date"]) ? trim($data["set_active_date"]) : date("Y-m-d H:i:s");
        $set_active_user_id = (int)$data["set_active_user_id"];
        $set_active_user_name = isset($data["set_active_user_name"]) ? trim($data["set_active_user_name"]) : "";

        $this->db->set(array(
            "id" => $id,
            "created" => $created,
            "creator_user_id" => $creator_user_id,
            "creator_user_name" => $creator_user_name,
            "name" => $name,
            "alamat" => $alamat,
            "province_id" => $province_id,
            "province_name" => $province_name,
            "regency_id" => $regency_id,
            "regency_name" => $regency_name,
            "district_id" => $district_id,
            "district_name" => $district_name,
            "village_id" => $village_id,
            "village_name" => $village_name,
            "is_active" => $is_active,
            "set_active_date" => $set_active_date,
            "set_active_user_id" => $set_active_user_id,
            "set_active_user_name" => $set_active_user_name,
        ));

        $this->db->replace($this->table_name);

        if ($id == 0) return $this->db->insert_id();
        return $id;
    }

    function perusahaan_delete($id = 0)
    {
        if ((int)$id == 0) return 0;

        $this->db->where("id", $id);
        $this->db->delete($this->table_name);

        return $id;
    }
    // ====================================================
    // ** END of OBJECT USER LEVEL
    // ====================================================
}
