<?php
class User_engine extends Db_engine
{
    private $user_login;
    private $user_level;
    private $perusahaan;
    public function __construct()
    {
        parent::__construct();
        $this->user_login = "user_login";
        $this->user_level = "user_login_level";
        $this->perusahaan = "perusahaan";
    }

    // ====================================================
    // ** OBJECT USER LOGIN
    // ====================================================
    function user_login_get_list($filters = array())
    {
        $this->db->select("$this->user_login.*, $this->user_level.name AS user_level_name, $this->perusahaan.name AS perusahaan_name");
        $this->db->from("$this->user_login");
        $this->db->join("$this->user_level", "$this->user_level.id = $this->user_login.user_level_id");
        $this->db->join("$this->perusahaan", "$this->perusahaan.id = $this->user_login.perusahaan_id");

        $this->db->where("$this->perusahaan.is_active", 1);

        foreach ($filters as $key => $value) {
            switch ($key) {
                case 'id':
                    $this->db->where("$this->user_login.id", (int)$value);
                    break;
                case 'hash_id':
                    $this->db->where("md5($this->user_login.id)", trim($value));
                    break;
                case 'username':
                    $this->db->where("$this->user_login.username", trim($value));
                    break;
                case 'email':
                    $this->db->where("$this->user_login.email", trim($value));
                    break;
                case 'password':
                    $this->db->where("$this->user_login.password", trim($value));
                    break;
                case 'user_level_id':
                    $this->db->where("$this->user_login.user_level_id", (int)$value);
                    break;
                case 'is_active':
                    $this->db->where("$this->user_login.is_active", (int)$value);
                    break;
                case 'is_archive':
                    $this->db->where("$this->user_login.is_archive", (int)$value);
                    break;
                case 'perusahaan_id':
                    $this->db->where("perusahaan_id", trim($value));
                    break;
                case 'perusahaan_hash_id':
                    $this->db->where("md5(perusahaan_id)", trim($value));
                    break;
                default:
                    break;
            }
        }

        $res = $this->db->get()->result_array();
        return $res;
    }

    function user_login_save($data = array())
    {
        $id = (int) $data["id"];
        $creator_user_id = (int) $data["creator_user_id"];
        $creator_user_name = trim($data["creator_user_name"]);
        $name = trim($data["name"]);
        $username = trim($data["username"]);
        $email = trim($data["email"]);
        $password = trim($data["password"]);
        $user_level_id = (int)$data["user_level_id"];
        $is_active = (int)$data["is_active"];
        $set_active_date = trim($data["set_active_date"]);
        $set_active_user_id = (int)$data["set_active_user_id"];
        $set_active_user_name = trim($data["set_active_user_name"]);
        $is_archive = (int)$data["is_archive"];
        $set_archive_date = trim($data["set_archive_date"]);
        $set_archive_user_id = (int)$data["set_archive_user_id"];
        $set_archive_user_name = trim($data["set_archive_user_name"]);
        $perusahaan_id = isset($data["perusahaan_id"]) ? (int) $data["perusahaan_id"] : 0;

        $this->db->set(array(
            "id" => $id,
            "created" => date("Y-m-d H:i:s"),
            "creator_user_id" => $creator_user_id,
            "creator_user_name" => $creator_user_name,
            "name" => $name,
            "username" => $username,
            "email" => $email,
            "password" => $password,
            "user_level_id" => $user_level_id,
            "is_active" => $is_active,
            "set_active_date" => $set_active_date,
            "set_active_user_id" => $set_active_user_id,
            "set_active_user_name" => $set_active_user_name,
            "is_archive" => $is_archive,
            "set_archive_date" => $set_archive_date,
            "set_archive_user_id" => $set_archive_user_id,
            "set_archive_user_name" => $set_archive_user_name,
            "perusahaan_id" => $perusahaan_id,
        ));

        $this->db->replace("user_login");

        if ($id == 0) return $this->db->insert_id();
        return $id;
    }

    function user_login_set_archive($data = array())
    {
        $is_archive = 1;

        $id = (int) $data["id"];
        $set_archive_user_id = (int) $data["set_archive_user_id"];
        $set_archive_user_name = trim($data["set_archive_user_name"]);

        if ($id == 0) return 0;

        $data = array(
            "is_archive" => $is_archive,
            "set_archive_date" => date("Y-m-d H:i:s"),
            "set_archive_user_id" => $set_archive_user_id,
            "set_archive_user_name" => $set_archive_user_name,
        );

        $this->db->where("id", $id);
        $this->db->update("user_login", $data);

        return $id;
    }

    function user_login_set_active($data = array())
    {
        $is_active = 1;
        $is_archive = 0;

        $id = (int) $data["id"];
        $set_active_user_id = (int) $data["set_active_user_id"];
        $set_active_user_name = trim($data["set_active_user_name"]);

        if ($id == 0) return 0;

        $data = array(
            "is_active" => $is_active,
            "set_active_date" => date("Y-m-d H:i:s"),
            "set_active_user_id" => $set_active_user_id,
            "set_active_user_name" => $set_active_user_name,
            "is_archive" => $is_archive,
            "set_archive_date" => date("Y-m-d H:i:s"),
            "set_archive_user_id" => $set_active_user_id,
            "set_archive_user_name" => $set_active_user_name,
        );

        $this->db->where("id", $id);
        $this->db->update("user_login", $data);

        return $id;
    }

    function user_login_delete($id = 0)
    {
        if ((int)$id == 0) return 0;

        $this->db->where("id", $id);
        $this->db->delete("user_login");

        return $id;
    }

    function user_login_set_password($data = array())
    {
        $id = (int) $data["id"];
        $password = trim($data["new_password"]);

        if ($id == 0) return 0;

        $data = array(
            "password" => $password,
        );

        $this->db->where("id", $id);
        $this->db->update("user_login", $data);

        return $id;
    }

    // ====================================================
    // ** END of OBJECT USER LOGIN
    // ====================================================

    // ====================================================
    // ** OBJECT USER LEVEL
    // ====================================================
    function user_level_get_list($filters = array())
    {
        $this->db->select("*");
        $this->db->from($this->user_level);

        foreach ($filters as $key => $value) {
            switch ($key) {
                case 'id':
                    $this->db->where("id", (int)$value);
                    break;
                case 'hash_id':
                    $this->db->where("md5(id)", trim($value));
                    break;
                case 'name':
                    $this->db->where("name", trim($value));
                    break;
                case 'perusahaan_id':
                    $this->db->where("perusahaan_id", trim($value));
                    break;
                default:
                    break;
            }
        }

        $res = $this->db->get()->result_array();
        return $res;
    }

    function user_level_save($data = array())
    {
        $id = (int) $data["id"];
        $creator_user_id = (int) $data["creator_user_id"];
        $creator_user_name = trim($data["creator_user_name"]);
        $name = trim($data["name"]);
        $allowed_json = trim($data["allowed_json"]);

        $this->db->set(array(
            "id" => $id,
            "created" => date("Y-m-d H:i:s"),
            "creator_user_id" => $creator_user_id,
            "creator_user_name" => $creator_user_name,
            "name" => $name,
            "allowed_json" => $allowed_json,
        ));

        $this->db->replace($this->user_level);

        if ($id == 0) return $this->db->insert_id();
        return $id;
    }

    function user_level_delete($id = 0)
    {
        if ((int)$id == 0) return 0;

        $this->db->where("id", $id);
        $this->db->delete($this->user_level);

        return $id;
    }
    // ====================================================
    // ** END of OBJECT USER LEVEL
    // ====================================================
}
