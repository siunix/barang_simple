<!DOCTYPE html>
<html lang="en">

<?php
$ucapan = "";
if (strtotime(date("H:i:s")) > strtotime((date("04:00:00")))) $ucapan = "Selamat Pagi";
if (strtotime(date("H:i:s")) > strtotime((date("12:00:00")))) $ucapan = "Selamat Siang";
if (strtotime(date("H:i:s")) > strtotime((date("15:00:00")))) $ucapan = "Selamat Sore";
if (strtotime(date("H:i:s")) > strtotime((date("18:30:00")))) $ucapan = "Selamat Malam";
if (strtotime(date("H:i:s")) > strtotime((date("00:00:00")))) $ucapan = "Selamat Malam";

$this->load->view("templates/head");
?>

<body>
	<div id="app">
		<section class="section">
			<div class="d-flex flex-wrap align-items-stretch">
				<div class="col-lg-4 col-md-12 col-12 order-lg-1 min-vh-100 order-2 bg-white">
					<div class="p-4 m-3 mt-5">
						<h2 class="text-dark font-weight-normal"><span class="font-weight-bold"><?= APP_NAME ?> - Login</span></h2>
						<h6 class="text-dark font-weight-normal"><?= APP_DESCRIPTION ?></h6>
						<form class="form-auth-small" onsubmit="return false" id=form-login>
							<div class="form-group">
								<label for="email">Email</label>
								<input id="email" type="email" class="form-control" name="email" id="email" tabindex="1" autofocus>
								<small class="text-info">email wajib diisi!</small>
								<div class="invalid-feedback">
									email tidak boleh kosong!
								</div>
							</div>

							<div class="form-group">
								<div class="d-block">
									<label for="password" class="control-label">Password</label>
								</div>
								<input id="password" type="password" class="form-control" name="password" id="password" tabindex="2">
								<small class="text-info">Password wajib diisi!</small>
								<div class="invalid-feedback">
									Password tidak boleh kosong!
								</div>
							</div>

							<div class="form-group text-right">
								<button onclick="do_login()" class="btn btn-primary btn-lg btn-block">Login</button>
							</div>

							<div class="absolute-bottom-left index-2">
								<div class="text-dark p-5 pb-2">
									<div class="text-small">
										Copyright &copy; 2020 - <?= APP_PROGRAMMER ? APP_PROGRAMMER : "" ?>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="col-lg-8 col-12 order-lg-2 order-1" data-background="<?= base_url('assets/img/bg01.jpg') ?>" style='background-size: cover'>
					<div class="absolute-bottom-left index-2">
						<div class="text-light p-5 pb-2">
							<div class="mb-5 pb-3">
								<h1 class="mb-2 display-4 text-dark font-weight-bold"><?= $ucapan ?></h1>
								<h5 class="font-weight-normal text-dark"></h5>
							</div>
							<div class="text-small text-secondary">
								Foto oleh Alina Blumberg dari Pexels<br>
								Template by Stisla
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

	<?php
	$this->load->view("templates/scripts");
	?>

	<!-- Ajax Script -->
	<script>
		function do_login() {
			var form_data = $("#form-login").serializeArray();
			ajax_post(
				'<?= base_url() ?>login/do_login',
				form_data,
				function(resp) {
					try {
						var json = JSON.parse(resp);
						var is_success = json.is_success;
						var message = json.message;
						if (is_success == 1) {
							show_toast("Success", "Redirect to dashboard ...", "success");
							setTimeout(function() {
								window.location.href = "<?= base_url() ?>";
							}, 2000);
						} else {
							show_toast("Error", json.message, "error");
						}
					} catch (error) {
						show_toast("Error", "Application response error", "error");
					}
				}
			);
		}
	</script>
</body>

</html>