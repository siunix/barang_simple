<?php
$session_data = $this->session->userdata(APP_SESSION_NAME);
$session_perusahaan_hash_id = isset($session_data["hash_id"]) ? $session_data["hash_id"] : "";
$is_allow_perusahaan = isset($session_data["allow_perusahaan"]) ? (int) $session_data["allow_perusahaan"] : "0";
$is_allow_perusahaan_create = isset($session_data["allow_perusahaan_create"]) ? (int) $session_data["allow_perusahaan_create"] : "0";
$is_allow_perusahaan_update = isset($session_data["allow_perusahaan_update"]) ? (int) $session_data["allow_perusahaan_update"] : "0";
$is_allow_perusahaan_delete = isset($session_data["allow_perusahaan_delete"]) ? (int) $session_data["allow_perusahaan_delete"] : "0";

if ($is_allow_perusahaan == 0) redirect_url("");

$title = "Tambah perusahaan";
$hash_id = "";
$name = "";
$alamat = "";
$province_hash_id = "";
$regency_hash_id = "";
$district_hash_id = "";
$village_hash_id = "";
$email = "";
$is_active = 0;
$checked = "";

if (count($detail) > 0) {
    $title = "Edit perusahaan";
    $hash_id = isset($detail["id"]) ? md5($detail["id"]) : "";
    $name = isset($detail["name"]) ? ($detail["name"]) : "";
    $alamat = isset($detail["alamat"]) ? ($detail["alamat"]) : "";
    $province_hash_id = isset($detail["province_id"]) ? md5($detail["province_id"]) : "";
    $regency_hash_id = isset($detail["regency_id"]) ? md5($detail["regency_id"]) : "";
    $district_hash_id = isset($detail["district_id"]) ? md5($detail["district_id"]) : "";
    $village_hash_id = isset($detail["village_id"]) ? md5($detail["village_id"]) : "";
    $email = isset($detail["email"]) ? $detail["email"] : "";
    $is_active = isset($detail["is_active"]) ? (int) $detail["is_active"] : 0;

    if ($is_active) $checked = "checked";
}

?>
<?php
if (!empty($hash_id) && !$is_active) {
?>
    <div class="alert alert-danger">
        <?= $name ?> tidak aktif
    </div>
<?php
}
?>
<section class="section mt-4">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">General</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <form onsubmit="return false" id="form-detail">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="section-body mt-4">
                    <input type="hidden" name="hash_id" value="<?= $hash_id ?>">

                    <div class="form-group">
                        <label for="name">Nama perusahaan</label>
                        <input type="text" id="name" name="name" class="form-control" value="<?= $name ?>" placeholder="Nama perusahaan">
                    </div>

                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <textarea rows="4" cols="" id="alamat" name="alamat" class="form-control"><?= $alamat ?></textarea>
                    </div>

                    <div class="form-group">
                        <label for="province">Province</label>
                        <select id="province" name="province" class="form-control" onchange="get_regency($(this).val())">
                            <option value="">-- Select Province --</option>
                            <?php
                            foreach ($province_list as $index => $value) {
                                $selected = "";
                                if ($value['id'] == $province_hash_id) $selected = "selected";
                            ?>
                                <option value="<?= $value['id'] ?>" <?= $selected ?>><?= $value['name'] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="regency">Regency</label>
                        <div id="regency_list">
                            <select id="regency" name="regency" class="form-control" onchange="get_district($(this).val())">
                                <option value="">-- Select regency --</option>
                                <?php
                                foreach ($regency_list as $index => $value) {
                                    $selected = "";
                                    if ($value['id'] == $regency_hash_id) $selected = "selected";
                                ?>
                                    <option value="<?= $value['id'] ?>" <?= $selected ?>><?= $value['name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="district">District</label>
                        <div id="district_list">
                            <select id="district" name="district" class="form-control" onchange="get_village($(this).val())">
                                <option value="">-- Select district --</option>
                                <?php
                                foreach ($district_list as $index => $value) {
                                    $selected = "";
                                    if ($value['id'] == $district_hash_id) $selected = "selected";
                                ?>
                                    <option value="<?= $value['id'] ?>" <?= $selected ?>><?= $value['name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="village">Village</label>
                        <div id="village_list">
                            <select id="village" name="village" class="form-control">
                                <option value="">-- Select village --</option>
                                <?php
                                foreach ($village_list as $index => $value) {
                                    $selected = "";
                                    if ($value['id'] == $village_hash_id) $selected = "selected";
                                ?>
                                    <option value="<?= $value['id'] ?>" <?= $selected ?>><?= $value['name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="checkbox" name="is_active" value="1" <?= $checked ?>>
                        <span class="ml-2">Active</span>
                    </div>
                </div>
            </div>
        </form>

        <hr>
        <?php if (!empty($hash_id) && $session_perusahaan_hash_id != $hash_id && $is_allow_perusahaan_delete == 1) : ?>
            <a href="javascript:void(0);" onclick="confirm_delete('<?= $hash_id ?>')" class="btn">Delete</a>
        <?php endif ?>
        <div class="pull-right">
            <?php if (($is_allow_perusahaan_create == 1 && empty($hash_id)) || $is_allow_perusahaan_update == 1) : ?>
                <button class="btn btn-primary" onclick="save()">Save</button>
            <?php endif ?>
        </div>
    </div>
</section>

<script>
    function get_regency(province_hash_id) {
        get_district();
        get_village();
        ajax_get(
            "<?= base_url("perusahaan/get_regencies/") ?>" + province_hash_id, {},
            function(resp) {
                $("#regency_list").html(resp);
            }
        );
    }

    function get_district(regency_hash_id) {
        get_village();
        ajax_get(
            "<?= base_url("perusahaan/get_districts/") ?>" + regency_hash_id, {},
            function(resp) {
                $("#district_list").html(resp);
            }
        );
    }

    function get_village(district_hash_id) {
        ajax_get(
            "<?= base_url("perusahaan/get_villages/") ?>" + district_hash_id, {},
            function(resp) {
                $("#village_list").html(resp);
            }
        );
    }
</script>