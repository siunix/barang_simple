<select id="district" name="district" class="form-control" onchange="get_village($(this).val())">
    <option value="">-- Select district --</option>
    <?php foreach ($district_list as $index => $value) : ?>
        <option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
    <?php endforeach ?>
</select>