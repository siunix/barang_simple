<select id="village" name="village" class="form-control">
    <option value="">-- Select village --</option>
    <?php foreach ($village_list as $index => $value) : ?>
        <option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
    <?php endforeach ?>
</select>