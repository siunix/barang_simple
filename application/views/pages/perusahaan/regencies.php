<select id="regency" name="regency" class="form-control" onchange="get_district($(this).val())">
    <option value="">-- Select regency --</option>
    <?php foreach ($regency_list as $index => $value) : ?>
        <option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
    <?php endforeach ?>
</select>