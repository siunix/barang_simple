<?php
if (count($list) > 0) {
    ?>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Perusahaan</th>
                    <th>User Access</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($list as $key => $value) : ?>
                    <tr>
                        <td><?= $value["name"] ?></td>
                        <td><?= $value["username"] ?></td>
                        <td><?= $value["perusahaan_name"] ?></td>
                        <td><?= $value["user_level_name"] ?></td>
                        <td>
                            <a href="javascript:void(0);" onclick="loadDetail('<?= $value['id'] ?>')" class="btn btn-outline-primary pull-right">Detail</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php
} else {
    echo "No data";
}
?>