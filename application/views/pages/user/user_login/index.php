<?php
$session_data = $this->session->userdata(APP_SESSION_NAME);
$is_allow_user_login     = isset($session_data["allow_user_login"]) ? (int)$session_data["allow_user_login"] : "0";
$is_allow_user_login_create = isset($session_data["allow_user_login_create"]) ? (int)$session_data["allow_user_login_create"] : "0";

if (!$is_allow_user_login) {
    # code...
    redirect_url("");
}

?>

<!doctype html>
<html lang="en">

<head>
    <?php
    $this->load->view("templates/head");
    ?>
</head>

<body>
    <div id="app">
        <div class="main-wrapper">
            <?php
            $this->load->view("templates/navbar");
            $this->load->view("templates/sidebar");
            ?>

            <!-- Main Content -->
            <div class="main-content">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <section class="section">
                            <div class="card">
                                <div class="card-header">
                                    <h4>User Login</h4>
                                    <div class="card-header-action">
                                        <?php if($is_allow_user_login_create == 1) : ?>
                                        <button class="btn btn-primary" type="button" id="new" onclick="loadDetail('')"><span class="lnr lnr-plus-circle"></span> New</button>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="section-body" id="list">
                
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12" id="content-body">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END WRAPPER -->
    <!-- Javascript -->
    <?php
    $this->load->view("templates/scripts");
    ?>
        <script>
        $(document).ready(function() {
            loadList();
        })

        function loadList() {
            ajax_get(
                "<?= base_url("user_login/ajax_list") ?>", {},
                function(resp) {
                    $("#back").hide();
                    $("#new").show();
                    $("#panel-title").text("User Level");
                    $("#list").html(resp);
                }
            );
        }

        function closeContentBody() {
            $("#content-body").hide();
            $("#new").show();
        }

        function loadDetail(hash_id) {
            ajax_get(
                "<?= base_url("user_login/ajax_detail/") ?>" + hash_id, {},
                function(resp) {
                    $("#back").show();
                    $("#content-body").show();
                    $("#panel-title").text("Add New");
                    $("#content-body").html(resp);
                }
            );
        }

        function save() {
            var form_data = $("#form-detail").serializeArray();
            ajax_post(
                "<?= base_url("user_login/ajax_save") ?>",
                form_data,
                function(resp) {
                    try {
                        var json = JSON.parse(resp);
                        if (json.is_success == 1) {
                            show_toast("Success", json.message, "success");
                            loadList();
                            loadDetail(json.id);
                        } else {
                            show_toast("Error", json.message, "error");
                        }
                    } catch (error) {
                        show_toast("Error", "Application response error");
                    }
                }
            );
        }

        function confirm_delete(hash_id) {
            var confirm_del = confirm("are you sure ?");
            if (!confirm_del) return false;

            ajax_get(
                "<?= base_url("user_login/ajax_delete/") ?>" + hash_id, {},
                function(resp) {
                    try {
                        var json = JSON.parse(resp);
                        if (json.is_success == 1) {
                            show_toast("Success", json.message, "success");
                            loadList();
                            $("#content-body").hide();
                        } else {
                            show_toast("Error", json.message, "error");
                        }
                    } catch (error) {
                        show_toast("Error", "Application response error", "error");
                    }
                }
            );
        }
    </script>
</body>

</html>