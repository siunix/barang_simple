<?php
$session_data = $this->session->userdata(APP_SESSION_NAME);
$session_user_login_hash_id = isset($session_data["hash_id"]) ? $session_data["hash_id"] : "";
$is_allow_user_login = isset($session_data["allow_user_login"]) ? (int) $session_data["allow_user_login"] : "0";
$is_allow_user_login_create = isset($session_data["allow_user_login_create"]) ? (int) $session_data["allow_user_login_create"] : "0";
$is_allow_user_login_update = isset($session_data["allow_user_login_update"]) ? (int) $session_data["allow_user_login_update"] : "0";
$is_allow_user_login_delete = isset($session_data["allow_user_login_delete"]) ? (int) $session_data["allow_user_login_delete"] : "0";

if ($is_allow_user_login == 0) redirect_url("");

$hash_id = "";
$name = "";
$username = "";
$email = "";
$password = "";
$confirm_password = "";
$user_level_hash_id = "";
$perusahaan_hash_id = "";
$is_active = 0;
$checked = "";
$checked_allow_master_data = "";

if (count($detail) > 0) {
    $hash_id = isset($detail["id"]) ? $detail["id"] : "";
    $name = isset($detail["name"]) ? ($detail["name"]) : "";
    $username = isset($detail["username"]) ? ($detail["username"]) : "";
    $email = isset($detail["email"]) ? ($detail["email"]) : "";
    $user_level_hash_id = isset($detail["user_level_id"]) ? $detail["user_level_id"] : "";
    $perusahaan_hash_id = isset($detail["perusahaan_id"]) ? $detail["perusahaan_id"] : "";
    $is_active = isset($detail["is_active"]) ? (int) $detail["is_active"] : 0;
    $allow_master_data = isset($detail["allow_master_data"]) ? (int) $detail["allow_master_data"] : 0;

    if ($is_active == 1) $checked = "checked";
    if ($allow_master_data == 1) $checked_allow_master_data = "checked";
}
?>

<section class="section">
    <div class="card">
        <div class="card-header">
            <h4>User Level</h4>
        </div>
        <div class="card-body">
            <div class="section-body">
                <form onsubmit="return false" id="form-detail">
                    <input type="hidden" name="hash_id" value="<?= $hash_id ?>">

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" id="name" name="name" class="form-control" value="<?= $name ?>" placeholder="e.g Abdillah F">
                    </div>

                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" id="username" name="username" class="form-control" value="<?= $username ?>" placeholder="e.g siunix">
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" id="email" name="email" class="form-control" value="<?= $email ?>" placeholder="e.g example@mail.com">
                    </div>

                    <div class="form-group">
                        <label for="perusahaan">Perusahaan</label>
                        <select id="perusahaan" name="perusahaan_hash_id" class="form-control">
                            <option value="">-- Select level --</option>
                            <?php
                            foreach ($perusahaan_list as $index => $list) {
                                $selected = "";
                                if ($perusahaan_hash_id == $list["id"]) $selected = "selected";
                                ?>
                                <option value="<?= $list["id"] ?>" <?= $selected ?>><?= $list["name"] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="user_level">User level</label>
                        <select id="user_level" name="user_level_hash_id" class="form-control">
                            <option value="">-- Select level --</option>
                            <?php
                            foreach ($user_level_list as $index => $list) {
                                $selected = "";
                                if ($user_level_hash_id == $list["id"]) $selected = "selected";
                                ?>
                                <option value="<?= $list["id"] ?>" <?= $selected ?>><?= $list["name"] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>

                    <?php if (empty($hash_id)) : ?>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" id="password" name="password" class="form-control" value="<?= $password ?>" placeholder="Password">
                        </div>

                        <div class="form-group">
                            <label for="confirm_password">Confirm password</label>
                            <input type="password" id="confirm_password" name="confirm_password" class="form-control" value="<?= $confirm_password ?>" placeholder="Confirm Password">
                        </div>
                    <?php endif ?>

                    <label class="fancy-checkbox">
                        <input type="checkbox" name="is_active" value="1" <?= $checked ?>>
                        <span>Active</span>
                    </label>
                </form>

                <hr>
                <?php if (!empty($hash_id) && $session_user_login_hash_id != $hash_id && $is_allow_user_login_delete == 1) : ?>
                    <a href="javascript:void(0);" onclick="confirm_delete('<?= $hash_id ?>')" class="btn">Delete</a>
                <?php endif ?>
                <div class="pull-right">
                <?php
                    $text = "Cancel";
                    ?>
                    <button class="btn btn-danger" onclick="closeContentBody()"><?= $text ?></button>
                    <?php if (($is_allow_user_login_create == 1 && empty($hash_id)) || $is_allow_user_login_update == 1) : ?>
                        <button class="btn btn-primary" onclick="save()">Save</button>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</section>