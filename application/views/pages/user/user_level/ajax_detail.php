<?php
$session_data = $this->session->userdata(APP_SESSION_NAME);
$user_level = isset($session_data["user_level"]) ? (int) $session_data["user_level"] : "0";
$is_allow_user_level = isset($session_data["allow_user_level"]) ? (int) $session_data["allow_user_level"] : "0";
$is_allow_user_level_create = isset($session_data["allow_user_level_create"]) ? (int) $session_data["allow_user_level_create"] : "0";
$is_allow_user_level_update = isset($session_data["allow_user_level_update"]) ? (int) $session_data["allow_user_level_update"] : "0";
$is_allow_user_level_delete = isset($session_data["allow_user_level_delete"]) ? (int) $session_data["allow_user_level_delete"] : "0";

if ($is_allow_user_level == 0) redirect_url("");

$hash_id = "";
$name = "";

$allow_user_login = 0;
$allow_user_login_create = 0;
$allow_user_login_update = 0;
$allow_user_login_delete = 0;

$allow_user_level = 0;
$allow_user_level_create = 0;
$allow_user_level_update = 0;
$allow_user_level_delete = 0;

$allow_perusahaan = 0;
$allow_perusahaan_create = 0;
$allow_perusahaan_update = 0;
$allow_perusahaan_delete = 0;

$allow_barang = 0;
$allow_barang_create = 0;
$allow_barang_update = 0;
$allow_barang_delete = 0;


if (count($detail) > 0) {
    $hash_id = isset($detail["id"]) ? md5($detail["id"]) : "";
    $name = isset($detail["name"]) ? ($detail["name"]) : "";

    $allowed = json_decode($detail["allowed_json"], true);

    $allow_user_login = isset($allowed["allow_user_login"]) ? $allowed["allow_user_login"] : 0;
    $allow_user_login_create = isset($allowed["allow_user_login_create"]) ? $allowed["allow_user_login_create"] : 0;
    $allow_user_login_update = isset($allowed["allow_user_login_update"]) ? $allowed["allow_user_login_update"] : 0;
    $allow_user_login_delete = isset($allowed["allow_user_login_delete"]) ? $allowed["allow_user_login_delete"] : 0;

    $allow_user_level = isset($allowed["allow_user_level"]) ? $allowed["allow_user_level"] : 0;
    $allow_user_level_create = isset($allowed["allow_user_level_create"]) ? $allowed["allow_user_level_create"] : 0;
    $allow_user_level_update = isset($allowed["allow_user_level_update"]) ? $allowed["allow_user_level_update"] : 0;
    $allow_user_level_delete = isset($allowed["allow_user_level_delete"]) ? $allowed["allow_user_level_delete"] : 0;

    $allow_perusahaan = isset($allowed["allow_perusahaan"]) ? $allowed["allow_perusahaan"] : 0;
    $allow_perusahaan_create = isset($allowed["allow_perusahaan_create"]) ? $allowed["allow_perusahaan_create"] : 0;
    $allow_perusahaan_update = isset($allowed["allow_perusahaan_update"]) ? $allowed["allow_perusahaan_update"] : 0;
    $allow_perusahaan_delete = isset($allowed["allow_perusahaan_delete"]) ? $allowed["allow_perusahaan_delete"] : 0;

    $allow_barang = isset($allowed["allow_barang"]) ? $allowed["allow_barang"] : 0;
    $allow_barang_create = isset($allowed["allow_barang_create"]) ? $allowed["allow_barang_create"] : 0;
    $allow_barang_update = isset($allowed["allow_barang_update"]) ? $allowed["allow_barang_update"] : 0;
    $allow_barang_delete = isset($allowed["allow_barang_delete"]) ? $allowed["allow_barang_delete"] : 0;
}
?>
<section class="section">
    <div class="card">
        <div class="card-header">
            <h4>User Level</h4>
        </div>
        <div class="card-body">
            <div class="section-body">
                <form onsubmit="return false" id="form-detail">
                    <input type="hidden" name="hash_id" value="<?= $hash_id ?>">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" id="name" name="name" class="form-control" value="<?= $name ?>" placeholder="Level name">
                    </div>
                    <hr>
                    <table class="table table-striped">
                        <tr>
                            <th width="5px">Access</th>
                            <th>Menu</th>
                        </tr>
                        <tr>
                            <th>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="allow_user_login" class="allow-user-login-parent" value="1" <?= $allow_user_login == 1 ? "checked" : "" ?> onchange="set_child_enabled('allow-user-login')">
                                </label>
                            </th>
                            <th>User Login</th>
                        </tr>
                        <tr>
                            <th>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="allow_user_login_create" class="allow-user-login-child" data-parent="allow_user_login" value="1" <?= $allow_user_login_create == 1 ? "checked" : "" ?> <?= $allow_user_login == 0 ? "disabled" : "" ?>>
                                </label>
                            </th>
                            <td>User Login Create</td>
                        </tr>
                        <tr>
                            <th>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="allow_user_login_update" class="allow-user-login-child" data-parent="allow_user_login" value="1" <?= $allow_user_login_update == 1 ? "checked" : "" ?> <?= $allow_user_login == 0 ? "disabled" : "" ?>>
                                </label>
                            </th>
                            <td>User Login Update</td>
                        </tr>
                        <tr>
                            <th>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="allow_user_login_delete" class="allow-user-login-child" data-parent="allow_user_login" value="1" <?= $allow_user_login_delete == 1 ? "checked" : "" ?> <?= $allow_user_login == 0 ? "disabled" : "" ?>>
                                </label>
                            </th>
                            <td>User Login Delete</td>
                        </tr>
                        <tr>
                            <th>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="allow_user_level" class="allow-user-level-parent" value="1" <?= $allow_user_level == 1 ? "checked" : "" ?> onchange="set_child_enabled('allow-user-level')">
                                </label>
                            </th>
                            <th>User level</th>
                        </tr>
                        <tr>
                            <th>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="allow_user_level_create" class="allow-user-level-child" data-parent="allow_user_level" value="1" <?= $allow_user_level_create == 1 ? "checked" : "" ?> <?= $allow_user_level == 0 ? "disabled" : "" ?>>
                                </label>
                            </th>
                            <td>User level Create</td>
                        </tr>
                        <tr>
                            <th>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="allow_user_level_update" class="allow-user-level-child" data-parent="allow_user_level" value="1" <?= $allow_user_level_update == 1 ? "checked" : "" ?> <?= $allow_user_level == 0 ? "disabled" : "" ?>>
                                </label>
                            </th>
                            <td>User level Update</td>
                        </tr>
                        <tr>
                            <th>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="allow_user_level_delete" class="allow-user-level-child" data-parent="allow_user_level" value="1" <?= $allow_user_level_delete == 1 ? "checked" : "" ?> <?= $allow_user_level == 0 ? "disabled" : "" ?>>
                                </label>
                            </th>
                            <td>User level Delete</td>
                        </tr>
                        <tr>
                            <th>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="allow_perusahaan" class="allow-perusahaan-parent" value="1" <?= $allow_perusahaan == 1 ? "checked" : "" ?> onchange="set_child_enabled('allow-perusahaan')">
                                </label>
                            </th>
                            <th>Perusahaan</th>
                        </tr>
                        <tr>
                            <th>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="allow_perusahaan_create" class="allow-perusahaan-child" data-parent="allow_perusahaan" value="1" <?= $allow_perusahaan_create == 1 ? "checked" : "" ?> <?= $allow_perusahaan == 0 ? "disabled" : "" ?>>
                                </label>
                            </th>
                            <td>Perusahaan Create</td>
                        </tr>
                        <tr>
                            <th>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="allow_perusahaan_update" class="allow-perusahaan-child" data-parent="allow_perusahaan" value="1" <?= $allow_perusahaan_update == 1 ? "checked" : "" ?> <?= $allow_perusahaan == 0 ? "disabled" : "" ?>>
                                </label>
                            </th>
                            <td>Perusahaan Update</td>
                        </tr>
                        <tr>
                            <th>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="allow_perusahaan_delete" class="allow-perusahaan-child" data-parent="allow_perusahaan" value="1" <?= $allow_perusahaan_delete == 1 ? "checked" : "" ?> <?= $allow_perusahaan == 0 ? "disabled" : "" ?>>
                                </label>
                            </th>
                            <td>Perusahaan Delete</td>
                        </tr>
                        <tr>
                            <th>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="allow_barang" class="allow-barang-parent" value="1" <?= $allow_barang == 1 ? "checked" : "" ?> onchange="set_child_enabled('allow-barang')">
                                </label>
                            </th>
                            <th>Barang</th>
                        </tr>
                        <tr>
                            <th>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="allow_barang_create" class="allow-barang-child" data-parent="allow_barang" value="1" <?= $allow_barang_create == 1 ? "checked" : "" ?> <?= $allow_barang == 0 ? "disabled" : "" ?>>
                                </label>
                            </th>
                            <td>Barang Create</td>
                        </tr>
                        <tr>
                            <th>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="allow_barang_update" class="allow-barang-child" data-parent="allow_barang" value="1" <?= $allow_barang_update == 1 ? "checked" : "" ?> <?= $allow_barang == 0 ? "disabled" : "" ?>>
                                </label>
                            </th>
                            <td>Barang Update</td>
                        </tr>
                        <tr>
                            <th>
                                <label class="fancy-checkbox">
                                    <input type="checkbox" name="allow_barang_delete" class="allow-barang-child" data-parent="allow_barang" value="1" <?= $allow_barang_delete == 1 ? "checked" : "" ?> <?= $allow_barang == 0 ? "disabled" : "" ?>>
                                </label>
                            </th>
                            <td>Barang Delete</td>
                        </tr>
                    </table>
                </form>
                <hr>
                <?php if ((!empty($hash_id) && $is_allow_user_level_delete == 1 || $hash_id != md5($user_level))) { ?>
                    <a href="javascript:void(0);" onclick="confirm_delete('<?= $hash_id ?>')" class="btn">Delete</a>
                <?php } ?>
                <div class="pull-right">
                    <?php
                    $text = "Cancel";
                    ?>
                    <button class="btn btn-danger" onclick="closeContentBody()"><?= $text ?></button>
                    <?php if (($is_allow_user_level_create == 1 && empty($hash_id)) || $is_allow_user_level_update == 1) : ?>
                        <button class="btn btn-primary" onclick="save()">Save</button>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function set_child_enabled(target) {
        var parent_checked = $("." + target + "-parent").prop("checked");
        $("." + target + "-child").prop("disabled", !parent_checked);
        if (parent_checked == false) {
            $("." + target + "-child").prop("checked", false);
        }

        $.each($("." + target + "-child"), function() {
            var childs_parent = $(this).data("parent");
            if (childs_parent != undefined) {
                target = childs_parent.split("_").join("-");
                parent_checked = $("." + target + "-parent").prop("checked");
                if (parent_checked == false) {
                    $("." + target + "-child").prop("disabled", true);
                }
            }
        });
    }
</script>