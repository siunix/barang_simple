<?php
if (count($list) > 0) {
    ?>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>Created</th>
                    <th>Level Name</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($list as $key => $value) : ?>
                    <tr>
                        <td><?= date("Y-m-d H:i:s", strtotime($value["created"])) ?></td>
                        <td><?= $value["name"] ?></td>
                        <td>
                            <a href="javascript:void(0);" onclick="loadDetail('<?= $value['id'] ?>')" class="btn btn-outline-primary pull-right">Detail</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php
} else {
    echo "No data";
}
?>