<?php
$session_data = $this->session->userdata(APP_SESSION_NAME);
$session_user_login_hash_id = isset($session_data["hash_id"]) ? $session_data["hash_id"] : "";
$fullname = isset($session_data["fullname"]) ? $session_data["fullname"] : "";
$username = isset($session_data["username"]) ? $session_data["username"] : "";
$email = isset($session_data["email"]) ? $session_data["email"] : "";
$user_level = isset($session_data["user_level"]) ? $session_data["user_level"] : "";
$user_level_name = isset($session_data["user_level_name"]) ? $session_data["user_level_name"] : "";
$allow_user_login = isset($session_data["allow_user_login"]) ? (int) $session_data["allow_user_login"] : "0";
$allow_user_menu = isset($session_data["allow_user_menu"]) ? (int) $session_data["allow_user_menu"] : "0";
$allow_user_level = isset($session_data["allow_user_level"]) ? (int) $session_data["allow_user_level"] : "0";
?>

<!doctype html>
<html lang="en">

<head>
    <?php
    $this->load->view("templates/head");
    ?>
</head>

<body>
    <div id="app">
        <div class="main-wrapper">
            <?php
            $this->load->view("templates/navbar");
            $this->load->view("templates/sidebar");
            ?>

            <!-- Main Content -->
            <div class="main-content">
                <section class="section">
                    <div class="section-header">
                        <h1>Dashboard</h1>
                    </div>

                    <div class="section-body">
                        <br>
                        <div class="alert alert-info">
                            Hi <?= $fullname ?>, you are logged in as <?= $user_level_name ?>
                        </div>
                        <?php
                        printr($session_data);
                        ?>
                    </div>
                </section>
            </div>
            <?php
            $this->load->view("templates/footer");
            ?>
        </div>
    </div>
    <!-- END WRAPPER -->
    <!-- Javascript -->
    <?php
    $this->load->view("templates/scripts");
    ?>
</body>

</html>