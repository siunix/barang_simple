<?php
$session_data = $this->session->userdata(APP_SESSION_NAME);
$session_barang_hash_id = isset($session_data["hash_id"]) ? $session_data["hash_id"] : "";
$is_allow_barang = isset($session_data["allow_barang"]) ? (int) $session_data["allow_barang"] : "0";
$is_allow_barang_create = isset($session_data["allow_barang_create"]) ? (int) $session_data["allow_barang_create"] : "0";
$is_allow_barang_update = isset($session_data["allow_barang_update"]) ? (int) $session_data["allow_barang_update"] : "0";
$is_allow_barang_delete = isset($session_data["allow_barang_delete"]) ? (int) $session_data["allow_barang_delete"] : "0";

if ($is_allow_barang == 0) redirect_url("");

$title = "Tambah barang";
$hash_id = "";
$name = "";
$jumlah = 0;
$code = "";
$perusahaan_hash_id = "";

if (count($detail) > 0) {
    $title = "Edit barang";
    $hash_id = isset($detail["id"]) ? md5($detail["id"]) : "";
    $code = isset($detail["code"]) ? $detail["code"] : "DIGENERATE OTOMATIS";
    $name = isset($detail["name"]) ? ($detail["name"]) : "";
    $jumlah = isset($detail["jumlah"]) ? ($detail["jumlah"]) : 0;
    $perusahaan_hash_id = isset($detail["perusahaan_id"]) ? md5($detail["perusahaan_id"]) : "";
}

?>
<section class="section mt-4">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">General</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <form onsubmit="return false" id="form-detail">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="section-body mt-4">
                    <input type="hidden" name="hash_id" value="<?= $hash_id ?>">

                    <div class="form-group">
                        <label for="code">Kode barang</label>
                        <input type="text" id="code" name="code" class="form-control" value="<?= $code ?>" placeholder="DIGENERATE OTOMATIS" disabled>
                    </div>

                    <div class="form-group">
                        <label for="name">Nama barang</label>
                        <input type="text" id="name" name="name" class="form-control" value="<?= $name ?>" placeholder="Nama barang">
                    </div>

                    <div class="form-group">
                        <label for="jumlah">Jumlah</label>
                        <input type="number" id="jumlah" name="jumlah" class="form-control" value="<?= $jumlah ?>" placeholder="Jumlah barang">
                    </div>

                    <div class="form-group">
                        <label for="perusahaan">Perusahaan</label>
                        <select id="perusahaan" name="perusahaan_hash_id" class="form-control">
                            <option value="">-- Select level --</option>
                            <?php
                            foreach ($perusahaan_list as $index => $list) {
                                $selected = "";
                                if ($perusahaan_hash_id == $list["id"]) $selected = "selected";
                                ?>
                                <option value="<?= $list["id"] ?>" <?= $selected ?>><?= $list["name"] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </form>

        <hr>
        <?php if (!empty($hash_id) && $session_barang_hash_id != $hash_id && $is_allow_barang_delete == 1) : ?>
            <a href="javascript:void(0);" onclick="confirm_delete('<?= $hash_id ?>')" class="btn">Delete</a>
        <?php endif ?>
        <div class="pull-right">
            <?php if (($is_allow_barang_create == 1 && empty($hash_id)) || $is_allow_barang_update == 1) : ?>
                <button class="btn btn-primary" onclick="save()">Save</button>
            <?php endif ?>
        </div>
    </div>
</section>