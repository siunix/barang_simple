<?php
$session_data = $this->session->userdata(APP_SESSION_NAME);
$is_allow_barang     = isset($session_data["allow_barang"]) ? (int)$session_data["allow_barang"] : "0";
$is_allow_barang_create = isset($session_data["allow_barang_create"]) ? (int)$session_data["allow_barang_create"] : "0";

if (!$is_allow_barang) {
    # code...
    redirect_url("");
}
?>

<!doctype html>
<html lang="en">

<head>
    <?php
    $this->load->view("templates/head");
    ?>
    <link href="<?= base_url("assets/vendor/datatables/datatables.min.css") ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url("assets/vendor/datatables/dataTables.bootstrap4.min.css") ?>" rel="stylesheet" type="text/css">
</head>

<body>
    <div id="app">
        <div class="main-wrapper">
            <?php
            $this->load->view("templates/navbar");
            $this->load->view("templates/sidebar");
            ?>

            <!-- Main Content -->
            <div class="main-content">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <section class="section">
                            <div class="card">
                                <div class="card-header">
                                    <h4 style="font-size:24px">Barang</h4>
                                    <div class="card-header-action">
                                        <?php if ($is_allow_barang_create == 1) : ?>
                                            <button class="btn btn-primary" type="button" id="new" onclick="loadDetail('')"><span class="lnr lnr-plus-circle"></span> New</button>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- <div class="col-lg-6 col-md-6 col-sm-12" id="content-body">

                    </div> -->
                </div>
                <section class="section">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <section class="section">
                                <div class="card card-primary">
                                    <div class="card-body">
                                        <div class="section-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped" id="datatable">
                                                    <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Created</th>
                                                            <th>Code</th>
                                                            <th>Name</th>
                                                            <th>Jumlah</th>
                                                            <th>Perusahaan</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <!-- <div class="col-lg-6 col-md-6 col-sm-12" id="content-body">

                    </div> -->
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="modal-detail">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="panel-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="content-body">

                </div>
            </div>
        </div>
    </div>
    <!-- END WRAPPER -->
    <!-- Javascript -->
    <?php
    $this->load->view("templates/scripts");
    ?>

    <script src="<?= base_url("assets/vendor/datatables/datatables.min.js") ?>"></script>
    <script src="<?= base_url("assets/vendor/datatables/dataTables.bootstrap4.min.js") ?>"></script>
    <script src="<?= base_url("assets/vendor/datatables/dataTables.select.min.js") ?>"></script>
    <script>
        var table, table_info;
        var current_page = 0;
        $(document).ready(function() {
            $("#panel-detail").hide();
            // load_list();
            table = $("#datatable").DataTable({

                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "responsive": false,
                //                "scrollY": "300px",
                //                "scrollCollapse": true,

                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo site_url('barang/list') ?>",
                    "type": "GET",
                    beforeSend: function(xhr) {
                        $("#loader").show();
                    },
                    complete: function(jqXHR, textStatus) {
                        $("#loader").hide();
                    },
                    statusCode: status_code,
                },

                // order
                "ordering": true,
                "order": [
                    [1, "asc"]
                ],
                //                "fixedColumns": true,

                //Set column definition initialisation properties.
                "columnDefs": [{
                        "targets": 0,
                        "orderable": false, //set not orderable,
                        "responsivePriority": 1,
                    },
                    {
                        "targets": 0,
                        "orderable": true, //set not orderable,
                        "responsivePriority": 2,
                        "data": null,
                        render: function(data, type, row) {
                            var html = data.no;
                            return html;
                        }
                    },
                    {
                        "targets": 1,
                        "orderable": true, //set not orderable,
                        "responsivePriority": 2,
                        "data": null,
                        render: function(data, type, row) {
                            var html = data.created;
                            return html;
                        }
                    },
                    {
                        "targets": 2,
                        "orderable": true, //set not orderable,
                        "responsivePriority": 2,
                        "data": null,
                        render: function(data, type, row) {
                            var html = data.code;
                            return html;
                        }
                    },
                    {
                        "targets": 3,
                        "orderable": true, //set not orderable,
                        "responsivePriority": 2,
                        "data": null,
                        render: function(data, type, row) {
                            var html = data.name;
                            return html;
                        }
                    },
                    {
                        "targets": 4,
                        "orderable": true, //set not orderable,
                        "responsivePriority": 2,
                        "data": null,
                        render: function(data, type, row) {
                            var html = data.jumlah;
                            return html;
                        }
                    },
                    {
                        "targets": 5,
                        "orderable": true, //set not orderable,
                        "responsivePriority": 2,
                        "data": null,
                        render: function(data, type, row) {
                            var html = data.perusahaan_name;
                            return html;
                        }
                    },
                    {
                        "targets": 6,
                        "orderable": true, //set not orderable,
                        "responsivePriority": 2,
                        "data": null,
                        render: function(data, type, row) {
                            var html = "<a data-toggle=\"tooltip\" data-placement=\"top\" class=\"btn btn-primary\" title data-original-title=\"Detail\" href=\"javascript:void(0);\" onclick=\"loadDetail('" + data.id + "')\" style=\"margin-right:10px\">Detail</a>";
                            return html;
                        }
                    },
                ],
                "columns": [{
                        "data": null
                    },
                    {
                        "data": null
                    },
                    {
                        "data": null
                    },
                    {
                        "data": null
                    },
                    {
                        "data": null
                    },
                    {
                        "data": null
                    }
                ],
                initComplete: function() {}
            });

            $('#datatable').on('page.dt', function() {
                var info = table.page.info();
                current_page = info.page;
            });

            // column search
            $('.search-input-text').unbind();
            $('.search-input-text').bind('keyup', function(e) {
                var i = $(this).attr('data-column'); // getting column index
                var v = $(this).val(); // getting search input value
                if (e.keyCode == 13 || v == "") {
                    table.columns(i).search(v).draw();
                }
            });

            $('.search-input-select').on('change', function() {
                var i = $(this).attr('data-column'); // getting column index
                var v = $(this).val(); // getting search input value
                table.columns(i).search(v).draw();
            });

            $("#datatable_filter input").unbind();
            $("#datatable_filter input").bind("keyup", function(e) {
                var v = this.value;
                if (e.keyCode == 13 || v == "") {
                    table.search(v).draw();
                }
            });
        })

        function loadList() {
            table.page(current_page).draw('page');
        }

        /*
        function loadList() {
            ajax_get(
                "<?= base_url("barang/list") ?>", {},
                function(resp) {
                    $("#back").hide();
                    $("#new").show();
                    $("#panel-title").text("Tahun Ajaran");
                    $("#list").html(resp);
                }
            );
        }
        */

        function loadDetail(hash_id) {
            ajax_get(
                "<?= base_url("barang/detail/") ?>" + hash_id, {},
                function(resp) {
                    if (hash_id == "") {
                        $("#panel-title").text("Add New");
                    } else {
                        $("#panel-title").text("Update barang");
                    }
                    $('#modal-detail').modal('show');
                    $("#content-body").html(resp);
                }
            );
        }

        function save() {
            var form_data = $("#form-detail").serializeArray();
            ajax_post(
                "<?= base_url("barang/save") ?>",
                form_data,
                function(resp) {
                    try {
                        var json = JSON.parse(resp);
                        if (json.is_success == 1) {
                            show_toast("Success", json.message, "success");
                            loadList();
                            loadDetail(json.id);
                        } else {
                            show_toast("Error", json.message, "error");
                        }
                    } catch (error) {
                        show_toast("Error", "Application response error");
                    }
                }
            );
        }

        function confirm_delete(hash_id) {
            var confirm_del = confirm("are you sure ?");
            if (!confirm_del) return false;

            ajax_get(
                "<?= base_url("barang/delete/") ?>" + hash_id, {},
                function(resp) {
                    try {
                        var json = JSON.parse(resp);
                        if (json.is_success == 1) {
                            show_toast("Success", json.message, "success");
                            loadList();
                            $('#modal-detail').modal('hide');
                            $("#content-body").hide();
                        } else {
                            show_toast("Error", json.message, "error");
                        }
                    } catch (error) {
                        show_toast("Error", "Application response error", "error");
                    }
                }
            );
        }
    </script>
</body>

</html>