<?php
$session_data = $this->session->userdata(APP_SESSION_NAME);
$session_user_login_hash_id = isset($session_data["hash_id"]) ? $session_data["hash_id"] : "";
$fullname = isset($session_data["fullname"]) ? $session_data["fullname"] : "";
$username = isset($session_data["username"]) ? $session_data["username"] : "";
$email = isset($session_data["email"]) ? $session_data["email"] : "";
$user_level = isset($session_data["user_level"]) ? $session_data["user_level"] : "";
$user_level_name = isset($session_data["user_level_name"]) ? $session_data["user_level_name"] : "";
$allow_user_login = isset($session_data["allow_user_login"]) ? (int) $session_data["allow_user_login"] : "0";
$allow_user_menu = isset($session_data["allow_user_menu"]) ? (int) $session_data["allow_user_menu"] : "0";
$allow_user_level = isset($session_data["allow_user_level"]) ? (int) $session_data["allow_user_level"] : "0";
?>

<div class="navbar-bg"></div>
<nav class="navbar navbar-expand-lg main-navbar">
    <form class="form-inline mr-auto">
        <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fa fa-bars"></i></a></li>
        </ul>
    </form>
    <ul class="navbar-nav navbar-right">
        <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                <img alt="image" src="<?= base_url('assets/img/avatar/avatar-1.png') ?>" class="rounded-circle mr-1">
                <div class="d-sm-none d-lg-inline-block"><?= strtoupper($fullname) ?></div>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-title"><?= strtolower($email) ?></div>
                <a href="<?= site_url("login/do_logout") ?>" class="dropdown-item has-icon text-danger">
                    <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                        <path fill="currentColor" d="M14.08,15.59L16.67,13H7V11H16.67L14.08,8.41L15.5,7L20.5,12L15.5,17L14.08,15.59M19,3A2,2 0 0,1 21,5V9.67L19,7.67V5H5V19H19V16.33L21,14.33V19A2,2 0 0,1 19,21H5C3.89,21 3,20.1 3,19V5C3,3.89 3.89,3 5,3H19Z" />
                    </svg> <span class="ml-2">Logout</span>
                </a>
            </div>
        </li>
    </ul>
</nav>