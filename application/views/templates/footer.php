<footer class="main-footer">
    <div class="footer-left">
        Copyright &copy; 2020 <div class="bullet"></div> <?= APP_NAME ?> - <?= APP_DESCRIPTION ?></a>
    </div>
    <div class="footer-right">
        <?= APP_PROGRAMMER ?>
    </div>
</footer>