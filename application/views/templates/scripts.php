<!-- JS Libraries -->
<script src="<?= base_url("assets/vendor/jquery/jquery.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/bootstrap/js/bootstrap.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/chartist/js/chartist.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/toastr/toastr.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/dropify/dropify.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/select2/select2.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/swal/sweetalert2.js") ?>"></script>
<script src="<?= base_url("assets/vendor/clock-picker/bootstrap-clockpicker.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/datepicker/bootstrap-datepicker.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/summernote/summernote-bs4.js") ?>"></script>

<!-- General JS Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>

<script src="<?= base_url('assets/js/stisla.js') ?>"></script>

<!-- Template JS File -->
<script src="<?= base_url('assets/js/scripts.js') ?>"></script>
<script src="<?= base_url('assets/js/custom.js') ?>"></script>
<!-- Page Specific JS File -->
<script>
    var status_code = {
        200: function() {},
        201: function(xhr) {},
        400: function(xhr) {
            try {
                var response_text = JSON.parse(xhr.responseText);
                show_toast(xhr.statusText, response_text.message, "error");
            } catch (error) {
                show_toast("Error", "Application Response Error", "error");
            }
        },
        401: function(xhr) {
            try {
                var response_text = JSON.parse(xhr.responseText);
                show_toast(xhr.statusText, response_text.message, "error");

                // window.location.href = '<?= base_url("login") ?>';
            } catch (error) {
                show_toast("Error", "Application Response Error", "error");
            }

        },
        402: function(xhr) {
            console.log(text + ", " + status + ", ")
        },
        403: function(xhr) {
            try {
                var response_text = JSON.parse(xhr.responseText);
                show_toast(xhr.statusText, response_text.message, "error");
            } catch (error) {
                show_toast("Error", "Application Response Error", "error");
            }
        },
        404: function(xhr) {
            try {
                var response_text = JSON.parse(xhr.responseText);
                show_toast(xhr.statusText, response_text.message, "error");
            } catch (error) {
                show_toast("Error", "Application Response Error", "error");
            }
        },
        405: function(xhr) {
            console.log(text + ", " + status + ", ")
        },
        500: function(xhr) {
            try {
                show_toast(xhr.statusText, "Server Response Error", "error");
            } catch (error) {
                show_toast("Error", "Application Response Error", "error");
            }
        }
    };

    function ajax_get_loader_text(url, data, target, text, resp_) {
        $.ajax({
            url: url,
            type: "GET",
            data: data,
            beforeSend: function(xhr) {
                $(target).html(text);
            },
            complete: function(jqXHR, textStatus) {},
            statusCode: status_code,
            success: function(resp, status) {
                resp_(resp);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $("#loader").hide();
            }
        });
    }

    function ajax_get(url, data, resp_) {
        $.ajax({
            url: url,
            type: "GET",
            data: data,
            beforeSend: function(xhr) {
                $("#loader").show();
            },
            complete: function(jqXHR, textStatus) {
                $("#loader").hide();
            },
            statusCode: status_code,
            success: function(resp, status) {
                resp_(resp);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $("#loader").hide();
            }
        });
    }

    function ajax_post(url, data, resp_) {
        var ajax = $.ajax({
            url: url,
            type: "POST",
            data: data,
            beforeSend: function(xhr) {
                $("#loader").show();
            },
            complete: function(jqXHR, textStatus) {
                $("#loader").hide();
            },
            statusCode: status_code,
            success: function(resp, status) {
                resp_(resp);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $("#loader").hide();
            }
        });
    }

    function ajax_post_file(url, data, resp_) {
        var ajax = $.ajax({
            url: url,
            type: "POST",
            enctype: 'multipart/form-data',
            data: data,
            processData: false, // Important!
            contentType: false,
            cache: false,
            timeout: 600000,
            beforeSend: function(xhr) {
                $("#loader").show();
            },
            complete: function(jqXHR, textStatus) {
                $("#loader").hide();
            },
            statusCode: status_code,
            success: function(resp, status) {
                resp_(resp);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $("#loader").hide();
            }
        });
    }

    function go_to_login_page() {
        window.location.href = "<?= base_url("login/do_logout") ?>";
    }

    function do_reset_password() {
        var form_data = $("#form-reset-password").serializeArray();
        ajax_post(
            "<?= base_url("user_login/ajax_do_reset_password") ?>",
            form_data,
            function(resp) {
                try {
                    var json = JSON.parse(resp);
                    if (json.is_success == 1) {
                        show_toast("Success", json.message, "success");
                        setTimeout(function() {
                            go_to_login_page();
                        }, 5300);
                    } else {
                        show_toast("Error", json.message, "error");
                    }
                } catch (error) {
                    show_toast("Error", "Application response error");
                }
            }
        );
    }

    function show_toast(title, message, type) {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        toastr[type](message, title);
    }

    function confirm_logout() {
        var confirm = alert("Are you sure want to logout ?");
        if (confirm) {
            go_to_login_page;
        } else {
            return false;
        }
    }
</script>