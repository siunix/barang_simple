<?php
$session_data = $this->session->userdata(APP_SESSION_NAME);
$session_user_login_hash_id = isset($session_data["hash_id"]) ? $session_data["hash_id"] : "";
$fullname = isset($session_data["fullname"]) ? $session_data["fullname"] : "";
$username = isset($session_data["username"]) ? $session_data["username"] : "";
$email = isset($session_data["email"]) ? $session_data["email"] : "";
$user_level = isset($session_data["user_level"]) ? $session_data["user_level"] : "";
$user_level_name = isset($session_data["user_level_name"]) ? $session_data["user_level_name"] : "";
$allow_user_login = isset($session_data["allow_user_login"]) ? (int) $session_data["allow_user_login"] : 0;
$allow_user_level = isset($session_data["allow_user_level"]) ? (int) $session_data["allow_user_level"] : 0;
$allow_perusahaan = isset($session_data["allow_perusahaan"]) ? (int) $session_data["allow_perusahaan"] : 0;
$allow_barang = isset($session_data["allow_barang"]) ? (int) $session_data["allow_barang"] : 0;

?>

<style>
    li>a.nav-link:hover {
        color: #6777ef
    }
</style>

<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="<?= base_url() ?>"><?= APP_NAME ?></a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="<?= base_url() ?>"><?= APP_INIT_NAME ?></a>
        </div>
        <ul class="sidebar-menu">

            <li><a class="nav-link" href="<?= site_url() ?>"><svg style="width:24px;height:24px" viewBox="0 0 24 24">
                        <path fill="currentColor" d="M19,5V7H15V5H19M9,5V11H5V5H9M19,13V19H15V13H19M9,17V19H5V17H9M21,3H13V9H21V3M11,3H3V13H11V3M21,11H13V21H21V11M11,15H3V21H11V15Z" />
                    </svg><span class="ml-4">Dashboard</span></a></li>

            <?php if ($allow_perusahaan) : ?>
                <li><a class="nav-link" href="<?= site_url('perusahaan') ?>"><svg style="width:24px;height:24px" viewBox="0 0 24 24">
                            <path fill="currentColor" d="M6 19H8V21H6V19M12 3L2 8V21H4V13H20V21H22V8L12 3M8 11H4V9H8V11M14 11H10V9H14V11M20 11H16V9H20V11M6 15H8V17H6V15M10 15H12V17H10V15M10 19H12V21H10V19M14 19H16V21H14V19Z" />
                        </svg><span class="ml-4">Perusahaan</span></a></li>
            <?php endif; ?>

            <?php if ($allow_barang) : ?>
                <li><a class="nav-link" href="<?= site_url('barang') ?>"><svg style="width:24px;height:24px" viewBox="0 0 24 24">
                            <path fill="currentColor" d="M6 19H8V21H6V19M12 3L2 8V21H4V13H20V21H22V8L12 3M8 11H4V9H8V11M14 11H10V9H14V11M20 11H16V9H20V11M6 15H8V17H6V15M10 15H12V17H10V15M10 19H12V21H10V19M14 19H16V21H14V19Z" />
                        </svg><span class="ml-4">Barang</span></a></li>
            <?php endif; ?>

            <?php if ($allow_user_login || $allow_user_level) : ?>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link has-dropdown"><svg style="width:24px;height:24px" viewBox="0 0 24 24">
                            <path fill="currentColor" d="M10 4A4 4 0 0 0 6 8A4 4 0 0 0 10 12A4 4 0 0 0 14 8A4 4 0 0 0 10 4M10 6A2 2 0 0 1 12 8A2 2 0 0 1 10 10A2 2 0 0 1 8 8A2 2 0 0 1 10 6M17 12C16.84 12 16.76 12.08 16.76 12.24L16.5 13.5C16.28 13.68 15.96 13.84 15.72 14L14.44 13.5C14.36 13.5 14.2 13.5 14.12 13.6L13.16 15.36C13.08 15.44 13.08 15.6 13.24 15.68L14.28 16.5V17.5L13.24 18.32C13.16 18.4 13.08 18.56 13.16 18.64L14.12 20.4C14.2 20.5 14.36 20.5 14.44 20.5L15.72 20C15.96 20.16 16.28 20.32 16.5 20.5L16.76 21.76C16.76 21.92 16.84 22 17 22H19C19.08 22 19.24 21.92 19.24 21.76L19.4 20.5C19.72 20.32 20.04 20.16 20.28 20L21.5 20.5C21.64 20.5 21.8 20.5 21.8 20.4L22.84 18.64C22.92 18.56 22.84 18.4 22.76 18.32L21.72 17.5V16.5L22.76 15.68C22.84 15.6 22.92 15.44 22.84 15.36L21.8 13.6C21.8 13.5 21.64 13.5 21.5 13.5L20.28 14C20.04 13.84 19.72 13.68 19.4 13.5L19.24 12.24C19.24 12.08 19.08 12 19 12H17M10 13C7.33 13 2 14.33 2 17V20H11.67C11.39 19.41 11.19 18.77 11.09 18.1H3.9V17C3.9 16.36 7.03 14.9 10 14.9C10.43 14.9 10.87 14.94 11.3 15C11.5 14.36 11.77 13.76 12.12 13.21C11.34 13.08 10.6 13 10 13M18.04 15.5C18.84 15.5 19.5 16.16 19.5 17.04C19.5 17.84 18.84 18.5 18.04 18.5C17.16 18.5 16.5 17.84 16.5 17.04C16.5 16.16 17.16 15.5 18.04 15.5Z" />
                        </svg><span class="ml-4">Setting</span></a>
                    <ul class="dropdown-menu">
                        <?php if ($allow_user_level) : ?>
                            <li><a class="nav-link" href="<?= site_url('user_level') ?>">User Level</a></li>
                        <?php endif; ?>
                        <?php if ($allow_user_login) : ?>
                            <li><a class="nav-link" href="<?= site_url('user_login') ?>">User Login</a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>

        </ul>
    </aside>
</div>