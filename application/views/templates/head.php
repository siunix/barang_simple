<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
	<title><?= APP_NAME ?></title>

	<!-- General CSS Files -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

	<!-- Vendor Library -->
	<link rel="stylesheet" href="<?= base_url("assets/vendor/linearicons/style.css") ?>">
	<link rel="stylesheet" href="<?= base_url("assets/vendor/chartist/css/chartist-custom.css") ?>">
	<link rel="stylesheet" href="<?= base_url("assets/vendor/toastr/toastr.css") ?>">
	<link rel="stylesheet" href="<?= base_url("assets/vendor/dropify/dropify.min.css") ?>">
	<link rel="stylesheet" href="<?= base_url("assets/vendor/select2/select2.min.css") ?>">
	<link rel="stylesheet" href="<?= base_url("assets/vendor/swal/sweetalert2.css") ?>">
	<link rel="stylesheet" href="<?= base_url("assets/vendor/clock-picker/bootstrap-clockpicker.min.css") ?>">
	<link rel="stylesheet" href="<?= base_url("assets/vendor/datepicker/bootstrap-datepicker3.min.css") ?>">
	<link rel="stylesheet" href="<?= base_url("assets/vendor/font-awesome/css/font-awesome.min.css") ?>">
	<link rel="stylesheet" href="<?= base_url("assets/vendor/summernote/summernote-bs4.css") ?>">

	<!-- Template CSS -->
	<link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/components.css') ?>">

	<style>
		.user-picture {
			width: 50px;
			height: 50px;
			-webkit-border-radius: 60px;
			-webkit-background-clip: padding-box;
			-moz-border-radius: 60px;
			-moz-background-clip: padding;
			border-radius: 60px;
			background-clip: padding-box;
			float: left;
			background-size: cover;
			background-position: center center;
		}

		.user-picture-detail {
			width: 150px;
			height: 200px;
			-webkit-border-radius: 60px;
			-webkit-background-clip: padding-box;
			-moz-border-radius: 60px;
			-moz-background-clip: padding;
			border-radius: 5px;
			background-clip: padding-box;
			float: left;
			background-size: cover;
			background-position: center center;
		}
	</style>

</head>