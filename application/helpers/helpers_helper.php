<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('printr')) {
    function printr($data) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }
}

if(!function_exists("get_all_method_for_each_controller")) {
    function get_all_method_for_each_controller($input_dir = "") {
        $input_dir = rtrim($input_dir, "/");
        $input_dir = ltrim($input_dir, "/");

        $comment_sign = "//--";

        // **
        // declaration of controller directory
        $controller_directory = "application/controllers/$input_dir";

        // **
        // get all file in controller directory
        $file_list = scandir($controller_directory);

        // ** 
        // get all controller file name and set as controller list name
        $controller_list = array();
        foreach ($file_list as $index => $file_name) {
            if($file_name == "." || $file_name == "..") continue;
            $explode_file_name = explode(".", $file_name);
            if(end($explode_file_name) != "php") continue;

            $controller_list[] = $file_name;
        }

        $final_controller_list = array();
        foreach ($controller_list as $index => $controller_file_name) {
            // **
            // get controller name
            $controller = str_replace(".php", "", $controller_file_name);

            $file_name = $controller_directory . "/$controller_file_name";
            $file = file($file_name);
            if($file) {
                $class_name = "";
                foreach ($file as $line_index => $line) {
                    if(empty($line)) continue;

                    if(strtolower(substr($line, 0, 5)) == "class") {
                        $class = $line;
                        $explode_class = explode(" ", $class);
                        $class_name = strtolower($explode_class[1]);
                    }

                    if(empty($class_name)) continue;

                    if(substr(trim($line), 0, 4) == $comment_sign) {
                        $function = $file[$line_index - 1];
                        $function = str_replace("function", "", $function);

                        $explode_function = explode("(", $function);
                        $function_name = trim($explode_function[0]);
                        $label = str_replace($comment_sign, "", trim($line));

                        $final_controller_list[$class_name][$function_name]['label'] = $label;
                        $final_controller_list[$class_name][$function_name]['allow'] = 0;
                    }
                }
            }
        }

        return $final_controller_list;
    }
}

if(!function_exists("set_http_response_success")) {
    function set_http_response_success($message = "Success", $data = array(), $id = "") {
        $response = array();
        $response["is_success"] = 1;
        $response["http_status_code"] = 200;
        $response["message"] = $message;
        $response["data"] = $data;
        $response["id"] = $id;

        http_response_code(200);

        return $response;
    }
}

if(!function_exists("set_http_response_error")) {
    function set_http_response_error($http_status_code = 0, $message = "Error", $data = array()) {
        $response = array();
        $response["is_success"] = 0;
        $response["http_status_code"] = $http_status_code;
        $response["message"] = $message;
        $response["data"] = $data;

        http_response_code($http_status_code);
        return $response;
    }
}

if(!function_exists("custom_password_encrypt")) {
    function custom_password_encrypt($password = "") {
        $pass = $password;
        $secretKey = ".SIUNIX";

        $pass_1 = sha1($secretKey);
        $pass_2 = md5($pass);
        $pass_3 = $pass_1."_SIUNIX_".$pass_2;
        $pass_4 = md5($pass_3);
        $final_pass = sha1(md5(sha1($pass_4)));

        return $final_pass;
    }
}

if(!function_exists("redirect_url")){
    function redirect_url($url){
        echo "<META HTTP-EQUIV=\"Refresh\" Content=\"0; URL=". base_url($url) ."\">";    
        exit;
    }
}

if(!function_exists("is_superadmin")){
    function is_superadmin(){
        $ci =& get_instance();
        $session_data = $ci->session->userdata(APP_SESSION_NAME);
        $user_level = $session_data["user_level"];
        if($user_level == -1) {
            return true;
        } else {
            return false;
        }
    }
}

if(!function_exists("get_uri_segment")){
    function get_uri_segment($segment = 0){
        if($segment == 0) return "";
        $ci =& get_instance();
        $segment_name = $ci->uri->segment("'".$segment."'");
        return $segment_name;
    }
}

if(!function_exists("get_system_config")){
    function get_system_config(){
        $ci =& get_instance();
        $db = $ci->db;

        $db->select("*");
        $db->from("system_config");

        $res = $db->get()->result_array();

        if(count($res) == 0) return array();

        $res = $res[0];

        return $res;
    }
}

if (!function_exists("generate_token")) {
    function generate_token()
    {
        $token = substr(str_shuffle(TOKEN_CHARACTER), -TOKEN_LENGTH);

        return $token;
    }
}