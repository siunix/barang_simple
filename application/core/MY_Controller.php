<?php

class MY_Controller extends CI_Controller
{

    private $is_ajax;

    public function __construct()
    {
        parent::__construct();
        $this->load->model("m_login");

        $this->is_ajax = false;

        if (isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && trim($_SERVER["HTTP_X_REQUESTED_WITH"]) == "XMLHttpRequest") {
            $this->is_ajax = true;
        }

        $this->check_log_in();
    }

    public function check_log_in()
    {
        if ($this->session->userdata(APP_SESSION_NAME) != null) {
            $session_data = $this->session->userdata(APP_SESSION_NAME);
            $email = $session_data["email"];
            $password = $session_data["password"];

            $res = $this->m_login->check_login($email, $password);
            if ($res["is_success"] == 0) {
                if (!$this->is_ajax) {
                    $this->m_login->do_logout();
                    redirect_url("login");
                }

                ob_clean();
                $res = set_http_response_error(401, "Please log in again");
                echo json_encode($res);
                exit;
            }

            return true;
        } else {
            redirect_url("login");
        }
    }
}
