<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    private $is_ajax;

    public function __construct()
    {
        parent::__construct();

        // **
        // load model
        $this->load->model("m_login");

        $this->is_ajax = false;
        if (isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && trim($_SERVER["HTTP_X_REQUESTED_WITH"]) == "XMLHttpRequest") {
            $this->is_ajax = true;
        }
    }

    public function index()
    {
        //-- Index

        if ($this->session->userdata(APP_SESSION_NAME) != null) {
            redirect_url("");
        }

        $data = array();

        $this->load->view("pages/login", $data);
    }

    function do_login()
    {
        //-- Do Login

        $email = $this->input->post("email");
        $password = $this->input->post("password");
        $password = custom_password_encrypt($password);
        $res = $this->m_login->check_login($email, $password);
        if (!$this->is_ajax) redirect_url("");

        ob_clean();
        echo json_encode($res);
    }

    function do_logout()
    {
        //-- Do Logout
        $this->m_login->do_logout();
        redirect_url("login");
    }
}
