<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_login extends MY_Controller
{

    private $is_ajax;

    // **
    // Structures
    // -- Contruct
    // -- Index
    // -- List
    // -- Detail
    // -- Save
    // -- Delete
    // -- Next Optional Method

    function __construct()
    {
        parent::__construct();

        $this->load->model("m_user");
        $this->load->model("m_perusahaan");

        $this->is_ajax = false;
        if (isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && trim($_SERVER["HTTP_X_REQUESTED_WITH"]) == "XMLHttpRequest") {
            $this->is_ajax = true;
        }
    }

    function index()
    {
        //-- Index
        $this->load->view("pages/user/user_login/index");
    }

    function ajax_list()
    {
        //-- List
        if ($this->is_ajax == false) redirect_url("");

        $data = array();
        $data["list"] = $this->m_user->user_login_get_list();

        $this->load->view("pages/user/user_login/ajax_list", $data);
    }

    function ajax_detail($hash_id = "")
    {
        //-- Detail
        if ($this->is_ajax == false) redirect_url("");

        $data = array();
        $data["detail"] = $this->m_user->user_login_get($hash_id);
        $data["user_level_list"] = $this->m_user->user_level_get_list();
        $data["perusahaan_list"] = $this->m_perusahaan->perusahaan_get_list();

        $this->load->view("pages/user/user_login/ajax_detail", $data);
    }

    function ajax_save()
    {
        //-- Save
        if ($this->is_ajax == false) redirect_url("");

        $res = $this->m_user->user_login_save();

        ob_clean();
        echo json_encode($res);
    }

    function ajax_delete($hash_id = "")
    {
        //-- Delete
        if ($this->is_ajax == false) redirect_url("");

        $res = $this->m_user->user_login_delete($hash_id);

        ob_clean();
        echo json_encode($res);
    }

    function ajax_do_reset_password()
    {
        //-- Reset Password
        if ($this->is_ajax == false) redirect_url("");

        $res = $this->m_user->do_reset_password();

        ob_clean();
        echo json_encode($res);
    }
}
