<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Perusahaan extends MY_Controller
{

    private $is_ajax;


    // **
    // Structures
    // -- Contruct
    // -- Index
    // -- List
    // -- Detail
    // -- Save
    // -- Delete
    // -- Next Optional Method

    function __construct()
    {
        parent::__construct();

        $this->load->model("m_area");
        $this->load->model("m_perusahaan");

        $this->is_ajax = false;
        if (isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && trim($_SERVER["HTTP_X_REQUESTED_WITH"]) == "XMLHttpRequest") {
            $this->is_ajax = true;
        }
    }

    function index()
    {
        //-- Index
        $this->load->view("pages/perusahaan/index");
    }

    function list()
    {
        //-- List
        $data           = $this->m_perusahaan->perusahaan_get_list($_GET, true);
        $rows_total     = $this->m_perusahaan->perusahaan_get_total();
        $filtered_total = $this->m_perusahaan->perusahaan_get_filtered_total($_GET);

        $table_data = array(
            "draw"            => isset($_GET["draw"]) ? (int) $_GET["draw"] : 1,
            "recordsTotal"    => $rows_total,
            "recordsFiltered" => $filtered_total,
            "data"            => $data
        );

        ob_clean();
        echo json_encode($table_data);
    }

    function detail($hash_id = "")
    {
        //-- Detail
        if ($this->is_ajax == false) redirect_url("");

        $province_id = 0;
        $regency_id = 0;
        $district_id = 0;

        $detail = $this->m_perusahaan->perusahaan_get($hash_id);

        if(count($detail) > 0) {
            $province_id = $detail["province_id"];
            $regency_id = $detail["regency_id"];
            $district_id = $detail["district_id"];
        }

        $data = array();
        $data["detail"] = $this->m_perusahaan->perusahaan_get($hash_id);
        $data["province_list"] = $this->m_area->provinces();
        $data["regency_list"] = $this->m_area->regencies(md5($province_id));
        $data["district_list"] = $this->m_area->districts(md5($regency_id));
        $data["village_list"] = $this->m_area->villages(md5($district_id));

        $this->load->view("pages/perusahaan/ajax_detail", $data);
    }

    function save()
    {
        //-- Save
        if ($this->is_ajax == false) redirect_url("");

        $res = $this->m_perusahaan->perusahaan_save();

        ob_clean();
        echo json_encode($res);
    }

    function delete($hash_id = "")
    {
        //-- Delete
        if ($this->is_ajax == false) redirect_url("");

        $res = $this->m_perusahaan->perusahaan_delete($hash_id);

        ob_clean();
        echo json_encode($res);
    }

    function get_regencies($province_hash_id) {
        if ($this->is_ajax == false) redirect_url("");

        $data = array();
        $data["regency_list"] = $this->m_area->regencies($province_hash_id);

        $this->load->view("pages/perusahaan/regencies", $data);
    }

    function get_districts($regency_hash_id) {
        if ($this->is_ajax == false) redirect_url("");

        $data = array();
        $data["district_list"] = $this->m_area->districts($regency_hash_id);

        $this->load->view("pages/perusahaan/districts", $data);
    }

    function get_villages($district_hash_id) {
        if ($this->is_ajax == false) redirect_url("");

        $data = array();
        $data["village_list"] = $this->m_area->villages($district_hash_id);

        $this->load->view("pages/perusahaan/villages", $data);
    }
}
