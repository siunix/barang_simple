<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barang extends MY_Controller
{

    private $is_ajax;


    // **
    // Structures
    // -- Contruct
    // -- Index
    // -- List
    // -- Detail
    // -- Save
    // -- Delete
    // -- Next Optional Method

    function __construct()
    {
        parent::__construct();

        $this->load->model("m_barang");
        $this->load->model("m_perusahaan");

        $this->is_ajax = false;
        if (isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && trim($_SERVER["HTTP_X_REQUESTED_WITH"]) == "XMLHttpRequest") {
            $this->is_ajax = true;
        }
    }

    function index()
    {
        //-- Index
        $this->load->view("pages/barang/index");
    }

    function list()
    {
        //-- List
        $data           = $this->m_barang->barang_get_list($_GET, true);
        $rows_total     = $this->m_barang->barang_get_total();
        $filtered_total = $this->m_barang->barang_get_filtered_total($_GET);

        $table_data = array(
            "draw"            => isset($_GET["draw"]) ? (int) $_GET["draw"] : 1,
            "recordsTotal"    => $rows_total,
            "recordsFiltered" => $filtered_total,
            "data"            => $data
        );

        ob_clean();
        echo json_encode($table_data);
    }

    function detail($hash_id = "")
    {
        //-- Detail
        if ($this->is_ajax == false) redirect_url("");

        $province_id = 0;
        $regency_id = 0;
        $district_id = 0;

        $detail = $this->m_barang->barang_get($hash_id);

        $data = array();
        $data["detail"] = $this->m_barang->barang_get($hash_id);
        $data["perusahaan_list"] = $this->m_perusahaan->perusahaan_get_list();

        $this->load->view("pages/barang/ajax_detail", $data);
    }

    function save()
    {
        //-- Save
        if ($this->is_ajax == false) redirect_url("");

        $res = $this->m_barang->barang_save();

        ob_clean();
        echo json_encode($res);
    }

    function delete($hash_id = "")
    {
        //-- Delete
        if ($this->is_ajax == false) redirect_url("");

        $res = $this->m_barang->barang_delete($hash_id);

        ob_clean();
        echo json_encode($res);
    }
}
