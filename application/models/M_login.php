<?php
class M_login extends CI_Model
{
    private $user_engine;
    private $perusahaan_engine;

    public function __construct()
    {
        parent::__construct();

        $this->user_engine = new User_engine();
        $this->perusahaan_engine = new Perusahaan_engine();
    }

    function check_login($email = "", $password = "")
    {
        if (empty($email)) return set_http_response_error(401, "Email tidak boleh kosong!");
        if (empty($password)) return set_http_response_error(401, "Password tidak boleh kosong!");

        $login_data = array(
            "email" => $email,
            "password" => $password,
        );

        $res = $this->db->get_where("user_login", $login_data)->result_array();
        if (count($res) == 0) return set_http_response_error(401, "Gagal Login, Username atau password salah!");

        $res = $res[0];
        $is_active = $res["is_active"];
        $is_archive = $res["is_archive"];
        $perusahaan_hash_id = md5($res["perusahaan_id"]);

        if ($is_active == 0) return set_http_response_error(401, "Gagal melakukan Login, Akun anda sudah tidak aktif!");
        if ($is_archive == 1) return set_http_response_error(401, "Gagal melakukan Login, Akun anda telah dihapus!");

        $session_data = array();
        $session_data["hash_id"] = md5($res["id"]);
        $session_data["perusahaan_hash_id"] = $perusahaan_hash_id;
        $session_data["name"] = trim($res["name"]);
        $session_data["username"] = trim($res["username"]);
        $session_data["email"] = trim($res["email"]);
        $session_data["password"] = trim($res["password"]);
        $session_data["user_level"] = (int) ($res["user_level_id"]);

        if ((int) $res["user_level_id"] == -1) {
            $session_data["user_level_name"] = "superadmin";
            $session_data["allow_user_login"] = 1;
            $session_data["allow_user_login_create"] = 1;
            $session_data["allow_user_login_update"] = 1;
            $session_data["allow_user_login_delete"] = 1;

            $session_data["allow_user_level"] = 1;
            $session_data["allow_user_level_create"] = 1;
            $session_data["allow_user_level_update"] = 1;
            $session_data["allow_user_level_delete"] = 1;

            $session_data["allow_perusahaan"] = 1;
            $session_data["allow_perusahaan_create"] = 1;
            $session_data["allow_perusahaan_update"] = 1;
            $session_data["allow_perusahaan_delete"] = 1;

            $session_data["allow_barang"] = 1;
            $session_data["allow_barang_create"] = 1;
            $session_data["allow_barang_update"] = 1;
            $session_data["allow_barang_delete"] = 1;

        } else {
            $filters = array();
            $filters["id"] = $session_data["user_level"];
            $res = $this->user_engine->user_level_get_list($filters);
            if (count($res) == 0) return set_http_response_error(401, "User level tidak dikenali");
            $res = $res[0];

            $session_data["user_level_name"] = $res["name"];
            $allowed = json_decode($res["allowed_json"], true);
            foreach ($allowed as $key => $value) {
                $session_data[$key] = $value;
            }

            // get perusahaan by perusahaan hash id
            $filters = array();
            $filters["hash_id"] = $perusahaan_hash_id;
            $res = $this->perusahaan_engine->perusahaan_get_list($filters);
            if (count($res) == 0) return set_http_response_error(401, "Perusahaan tidak dikenali!");
            $res = $res[0];

            $perusahaan_is_active = (int) $res['is_active'];
            if(!$perusahaan_is_active) return set_http_response_error(401, "Perusahaan tidak aktif!");
        }

        // **
        // set_session
        $this->session->set_userdata(APP_SESSION_NAME, $session_data);
        return set_http_response_success("Success");
    }

    function do_logout()
    {
        $this->session->unset_userdata(APP_SESSION_NAME);
    }
}
