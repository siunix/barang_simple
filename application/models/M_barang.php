<?php
class M_barang extends MY_Model
{
    private $user_engine;
    private $barang_engine;
    private $perusahaan_engine;

    private $creator_user_hash_id;
    private $creator_username;
    private $perusahaan_hash_id;

    private $is_allow_barang;
    private $is_allow_barang_create;
    private $is_allow_barang_update;
    private $is_allow_barang_delete;

    public function __construct()
    {
        parent::__construct();

        $this->user_engine = new User_engine();
        $this->barang_engine = new Barang_engine();
        $this->perusahaan_engine = new Perusahaan_engine();

        $session_data = $this->session->userdata(APP_SESSION_NAME);

        $this->is_allow_barang = isset($session_data["allow_barang"]) ? (int) $session_data["allow_barang"] : 0;
        $this->is_allow_barang_create = isset($session_data["allow_barang_create"]) ? (int) $session_data["allow_barang_create"] : 0;
        $this->is_allow_barang_update = isset($session_data["allow_barang_update"]) ? (int) $session_data["allow_barang_update"] : 0;
        $this->is_allow_barang_delete = isset($session_data["allow_barang_delete"]) ? (int) $session_data["allow_barang_delete"] : 0;

        if ($this->is_allow_barang == 0) {
            $this->is_allow_barang_create = 0;
            $this->is_allow_barang_update = 0;
            $this->is_allow_barang_delete = 0;
        }

        $this->creator_user_hash_id = $session_data["hash_id"];
        $this->creator_username = $session_data["username"];
        $this->perusahaan_hash_id = $session_data["perusahaan_hash_id"];
    }

    function barang_get_list($filters = array())
    {
        if ($this->is_allow_barang == 0) return array();

        if (!is_superadmin()) {
            $perusahaan_filters = array();
            $perusahaan_filters['hash_id'] = $this->perusahaan_hash_id;
            $res = $this->perusahaan_engine->perusahaan_get_list($perusahaan_filters);
            if (count($res) == 0) return array();
            $res = $res[0];
            $perusahaan_id = $res['id'];

            $filters['perusahaan_id'] = $perusahaan_id;
        }

        $res = $this->barang_engine->barang_get_list($filters, true, true);

        $final_res = array();
        $no = isset($filters["start"]) ? (int) $filters["start"] : 0;
        foreach ($res as $index => $data) {
            $row = array();

            $no++;

            $row['no'] = $no;
            $row['id'] = md5($data["id"]);
            $row['created'] = trim($data["created"]);
            $row['creator_user_name'] = trim($data["creator_user_name"]);
            $row['code'] = trim($data["code"]);
            $row['name'] = trim($data["name"]);
            $row['jumlah'] = trim($data["jumlah"]);
            $row['perusahaan_name'] = trim($data["perusahaan_name"]);

            $final_res[] = $row;
        }

        return $final_res;
    }

    function barang_get_total($filters = array())
    {
        $res = $this->barang_engine->barang_get_list($filters);
        return count($res);
    }

    function barang_get_filtered_total($filters = array())
    {
        $res = $this->barang_engine->barang_get_list($filters, false, true);
        return count($res);
    }

    function barang_get($hash_id = "")
    {
        if ($this->is_allow_barang == 0) return array();

        if (empty($hash_id)) return array();

        $filters = array();
        $filters["hash_id"] = $hash_id;
        $res = $this->barang_engine->barang_get_list($filters);

        if (count($res) == 0) return array();
        $res = $res[0];

        return $res;
    }

    function barang_save()
    {
        $id = 0;

        $hash_id = $this->input->post("hash_id");
        $name = $this->input->post("name");
        $jumlah = $this->input->post("jumlah");
        $perusahaan_hash_id = !empty($this->input->post("perusahaan_hash_id")) ? $this->input->post("perusahaan_hash_id") : "";

        if (empty($name)) return set_http_response_error(HTTP_BAD_REQUEST, "Nama barang is required");
        if (empty($jumlah)) return set_http_response_error(HTTP_BAD_REQUEST, "Jumlah Barang harus diisi dengan angka dan tidak boleh kosong");
        if (empty($perusahaan_hash_id)) return set_http_response_error(HTTP_BAD_REQUEST, "Perusahaan is required");

        // **
        // get user data by user hash_id
        $perusahaan_id = 0;
        if (!is_superadmin()) {
            $filters = array();
            $filters["hash_id"] = $this->creator_user_hash_id;
            $res = $this->user_engine->user_login_get_list($filters);
            if (count($res) == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "You don't have permission to access this modul");
            $res = $res[0];
            $creator_user_id = $res["id"];
            $creator_user_name = $res["username"];

            $filters = array();
            $filters['hash_id'] = $this->perusahaan_hash_id;
            $res = $this->perusahaan_engine->perusahaan_get_list($filters);
            if (count($res) == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "You don't have permission to access this modul");
            $res = $res[0];
            $perusahaan_id = $res['id'];
        } else {
            $creator_user_id = -1;
            $creator_user_name = $this->creator_username;
        }

        $filters_perusahaan = array();
        $filters_perusahaan['hash_id'] = $perusahaan_hash_id;
        $res = $this->perusahaan_engine->perusahaan_get_list($filters_perusahaan);
        if (count($res) == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "perusahaan tidak ditemukan");
        $res = $res[0];

        $perusahaan_id = $res['id'];
        $perusahaan_name = $res['name'];

        // Jika edit (hash_id != "")
        // check dan validasi id
        if (!empty($hash_id)) {
            if ($this->is_allow_barang_update == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "You don't have permission to update data");

            $filters = array();
            $filters["hash_id"] = $hash_id;
            $res = $this->barang_engine->barang_get_list($filters);
            if (count($res) == 0) return set_http_response_error(HTTP_BAD_REQUEST, "Data barang tidak ditemukan");
            $res = $res[0];
            $id = $res["id"];
            $created = $res["created"];
            $code = $res["code"];

            // check duplicate code
            $filters = array();
            $filters['code'] = $code;
            $res = $this->barang_engine->barang_get_list($filters);
            if (count($res) > 0) {
                $res = $res[0];
                $current_hash_id = md5($res['id']);
                if ($hash_id != $current_hash_id) return set_http_response_error(HTTP_BAD_REQUEST, "Nama barang telah terdaftar");
            }
        } else {
            if ($this->is_allow_barang_create == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "you don't have permission to create data");
            $created = date("Y-m-d H:i:s");
        }

        // check duplicate name
        $filters = array();
        $filters['name'] = $name;
        $filters['perusahaan_hash_id'] = $this->perusahaan_hash_id;
        $res = $this->barang_engine->barang_get_list($filters);
        if (count($res) > 0) {
            $res = $res[0];
            $current_hash_id = md5($res['id']);
            if ($hash_id != $current_hash_id) return set_http_response_error(HTTP_BAD_REQUEST, "Nama barang telah terdaftar");
        }

        $code = "BR-".strtotime(date("Y-m-d H:i:s"));

        $save_data = array();
        $save_data["id"] = (int) $id;
        $save_data["created"] = !empty($created) ? trim($created) : date("Y-m-d H:i:s");
        $save_data["creator_user_id"] = !empty($creator_user_id) ? (int) $creator_user_id : 0;
        $save_data["creator_user_name"] = !empty($creator_user_name) ? trim($creator_user_name) : "";
        $save_data["name"] = !empty($name) ? trim($name) : "";
        $save_data["code"] = !empty($code) ? trim($code) : "";
        $save_data["jumlah"] = !empty($jumlah) ? $jumlah : 0;
        $save_data["perusahaan_id"] = !empty($perusahaan_id) ? $perusahaan_id : 0;

        $this->db->trans_begin();
        try {
            $res = $this->barang_engine->barang_save($save_data);
            if ((int) $res == 0) {
                $this->db->trans_rollback();
                return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Failed to savess");
            }

            $this->db->trans_commit();
            return set_http_response_success("Success", array(), md5($res));
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Failed to save");
        }
    }

    function barang_delete($hash_id = "")
    {
        if ($this->is_allow_barang_delete == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "You don't have permission to delete data");
        if (empty($hash_id)) return set_http_response_error(HTTP_BAD_REQUEST, "Failed to delete");

        // **
        // Check hash_id
        $filters = array();
        $filters["hash_id"] = $hash_id;
        $res = $this->barang_engine->barang_get_list($filters);
        if (count($res) == 0) return set_http_response_error(HTTP_BAD_REQUEST, "Failed to delete");
        $res = $res[0];
        $id = (int) $res["id"];

        $this->db->trans_begin();
        try {
            $res = $this->barang_engine->barang_delete($id);
            if ((int) $res == 0) {
                $this->db->trans_rollback();
                return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Failed to delete");
            }

            $this->db->trans_commit();
            return set_http_response_success("Success");
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Fail to delete");
        }
    }
}
