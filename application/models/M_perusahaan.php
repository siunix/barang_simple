<?php
class M_perusahaan extends MY_Model
{
    private $user_engine;
    private $area_engine;
    private $perusahaan_engine;
    private $creator_user_hash_id;
    private $creator_username;
    private $perusahaan_hash_id;

    private $is_allow_perusahaan;
    private $is_allow_perusahaan_create;
    private $is_allow_perusahaan_update;
    private $is_allow_perusahaan_delete;

    public function __construct()
    {
        parent::__construct();

        $this->user_engine = new User_engine();
        $this->area_engine = new Area_engine();
        $this->perusahaan_engine = new Perusahaan_engine();

        $session_data = $this->session->userdata(APP_SESSION_NAME);

        $this->is_allow_perusahaan = isset($session_data["allow_perusahaan"]) ? (int) $session_data["allow_perusahaan"] : 0;
        $this->is_allow_perusahaan_create = isset($session_data["allow_perusahaan_create"]) ? (int) $session_data["allow_perusahaan_create"] : 0;
        $this->is_allow_perusahaan_update = isset($session_data["allow_perusahaan_update"]) ? (int) $session_data["allow_perusahaan_update"] : 0;
        $this->is_allow_perusahaan_delete = isset($session_data["allow_perusahaan_delete"]) ? (int) $session_data["allow_perusahaan_delete"] : 0;

        if ($this->is_allow_perusahaan == 0) {
            $this->is_allow_perusahaan_create = 0;
            $this->is_allow_perusahaan_update = 0;
            $this->is_allow_perusahaan_delete = 0;
        }

        $this->creator_user_hash_id = $session_data["hash_id"];
        $this->creator_username = $session_data["username"];
        $this->perusahaan_hash_id = $session_data["perusahaan_hash_id"];
    }

    function perusahaan_get_list($filters = array(), $datatables=false)
    {
        if ($this->is_allow_perusahaan == 0) return array();

        $filters['is_active'] = 1;
        if(!is_superadmin() || !$this->is_allow_perusahaan || !$this->is_allow_perusahaan_create || !$this->is_allow_perusahaan_update || !$this->is_allow_perusahaan_delete){
            $filters['hash_id'] = $this->perusahaan_hash_id;
        }

        if($datatables) {
            $res = $this->perusahaan_engine->perusahaan_get_list($filters, true, true);
        } else {
            $res = $this->perusahaan_engine->perusahaan_get_list($filters);
        }

        $final_res = array();
        $no = isset($filters["start"]) ? (int) $filters["start"] : 0;
        foreach ($res as $index => $data) {
            $row = array();

            $no++;

            $row['no'] = $no;
            $row['id'] = md5($data["id"]);
            $row['created'] = trim($data["created"]);
            $row['creator_user_name'] = trim($data["creator_user_name"]);
            $row['name'] = trim($data["name"]);
            $row['alamat'] = trim($data["alamat"]);
            $row['province_name'] = trim($data["province_name"]);
            $row['regency_name'] = trim($data["regency_name"]);
            $row['district_name'] = trim($data["district_name"]);
            $row['village_name'] = trim($data["village_name"]);
            $row['active'] = (int) $data["is_active"];

            $final_res[] = $row;
        }

        return $final_res;
    }

    function perusahaan_get_total($filters = array())
    {
        $res = $this->perusahaan_engine->perusahaan_get_list($filters);
        return count($res);
    }

    function perusahaan_get_filtered_total($filters = array())
    {
        $res = $this->perusahaan_engine->perusahaan_get_list($filters, false, true);
        return count($res);
    }

    function perusahaan_get($hash_id = "")
    {
        if ($this->is_allow_perusahaan == 0) return array();

        if (empty($hash_id)) return array();

        $filters = array();
        $filters["hash_id"] = $hash_id;
        $res = $this->perusahaan_engine->perusahaan_get_list($filters);

        if (count($res) == 0) return array();
        $res = $res[0];

        return $res;
    }

    function perusahaan_save()
    {
        $id = 0;

        $hash_id = $this->input->post("hash_id");
        $name = $this->input->post("name");
        $alamat = $this->input->post("alamat");
        $province_hash_id = $this->input->post("province");
        $regency_hash_id = $this->input->post("regency");
        $district_hash_id = $this->input->post("district");
        $village_hash_id = $this->input->post("village");
        $is_active = (int) $this->input->post("is_active");

        if (!in_array($is_active, array(0, 1))) return set_http_response_error(HTTP_BAD_REQUEST, "Bad request");

        if (empty($name)) return set_http_response_error(HTTP_BAD_REQUEST, "Nama perusahaan is required");
        if (empty($alamat)) return set_http_response_error(HTTP_BAD_REQUEST, "Alamat is required");
        if (empty($province_hash_id)) return set_http_response_error(HTTP_BAD_REQUEST, "Provice is required");
        if (empty($regency_hash_id)) return set_http_response_error(HTTP_BAD_REQUEST, "Regency is required");
        if (empty($district_hash_id)) return set_http_response_error(HTTP_BAD_REQUEST, "District is required");
        if (empty($village_hash_id)) return set_http_response_error(HTTP_BAD_REQUEST, "Village is required");

        // **
        // get user data by user hash_id
        if (!is_superadmin()) {
            $filters = array();
            $filters["hash_id"] = $this->creator_user_hash_id;
            $res = $this->user_engine->user_login_get_list($filters);
            if (count($res) == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "You don't have permission to access this modul");
            $res = $res[0];
            $creator_user_id = $res["id"];
            $creator_user_name = $res["username"];
        } else {
            $creator_user_id = -1;
            $creator_user_name = $this->creator_username;
        }

        // Jika edit (hash_id != "")
        // check dan validasi id
        if (!empty($hash_id)) {
            if ($this->is_allow_perusahaan_update == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "You don't have permission to update data");

            $filters = array();
            $filters["hash_id"] = $hash_id;
            $res = $this->perusahaan_engine->perusahaan_get_list($filters);

            if (count($res) == 0) return set_http_response_error(HTTP_BAD_REQUEST, "Data perusahaan tidak ditemukan");
            $res = $res[0];
            $id = $res["id"];
            $created = $res["created"];
        } else {
            if ($this->is_allow_perusahaan_create == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "you don't have permission to create data");
            $created = date("Y-m-d H:i:s");
        }

        // check duplicate name
        $filters = array();
        $filters['name'] = $name;
        $res = $this->perusahaan_engine->perusahaan_get_list($filters);
        if (count($res) > 0) {
            $res = $res[0];
            $current_hash_id = md5($res['id']);
            if ($hash_id != $current_hash_id) return set_http_response_error(HTTP_BAD_REQUEST, "Nama Perusahaan telah terdaftar");
        }

        // get province data
        $filters = array();
        $filters["hash_id"] = $province_hash_id;
        $res = $this->area_engine->province_get_list($filters);
        if (count($res) == 0) return set_http_response_error(HTTP_BAD_REQUEST, "Province tidak terdaftar");
        $res = $res[0];
        $province_id = $res["id"];
        $province_name = $res["name"];

        // get regency data
        $filters = array();
        $filters["hash_id"] = $regency_hash_id;
        $res = $this->area_engine->regency_get_list($filters);
        if (count($res) == 0) return set_http_response_error(HTTP_BAD_REQUEST, "Regency tidak terdaftar");
        $res = $res[0];
        $regency_id = $res["id"];
        $regency_name = $res["name"];

        // get district data
        $filters = array();
        $filters["hash_id"] = $district_hash_id;
        $res = $this->area_engine->district_get_list($filters);
        if (count($res) == 0) return set_http_response_error(HTTP_BAD_REQUEST, "district tidak terdaftar");
        $res = $res[0];
        $district_id = $res["id"];
        $district_name = $res["name"];

        // get village data
        $filters = array();
        $filters["hash_id"] = $village_hash_id;
        $res = $this->area_engine->village_get_list($filters);
        if (count($res) == 0) return set_http_response_error(HTTP_BAD_REQUEST, "village tidak terdaftar");
        $res = $res[0];
        $village_id = $res["id"];
        $village_name = $res["name"];

        $save_data = array();
        $save_data["id"] = (int) $id;
        $save_data["created"] = !empty($created) ? trim($created) : date("Y-m-d H:i:s");
        $save_data["creator_user_id"] = !empty($creator_user_id) ? (int) $creator_user_id : 0;
        $save_data["creator_user_name"] = !empty($creator_user_name) ? trim($creator_user_name) : "";
        $save_data["name"] = !empty($name) ? trim($name) : "";
        $save_data["alamat"] = !empty($alamat) ? trim($alamat) : "";
        $save_data['province_id'] = !empty($province_id) ? (int) $province_id : 0;
        $save_data['province_name'] = !empty($province_name) ? trim($province_name) : "";
        $save_data['regency_id'] = !empty($regency_id) ? (int) $regency_id : 0;
        $save_data['regency_name'] = !empty($regency_name) ? trim($regency_name) : "";
        $save_data['district_id'] = !empty($district_id) ? (int) $district_id : 0;
        $save_data['district_name'] = !empty($district_name) ? trim($district_name) : "";
        $save_data['village_id'] = !empty($village_id) ? (int) $village_id : 0;
        $save_data['village_name'] = !empty($village_name) ? trim($village_name) : "";
        $save_data['is_active'] = !empty($is_active) ? (int) $is_active : 0;
        $save_data['set_active_date'] = date("Y-m-d H:i:s");
        $save_data["set_active_user_id"] = !empty($creator_user_id) ? (int) $creator_user_id : 0;
        $save_data["set_active_user_name"] = !empty($creator_user_name) ? trim($creator_user_name) : "";

        $this->db->trans_begin();
        try {
            $res = $this->perusahaan_engine->perusahaan_save($save_data);
            if ((int) $res == 0) {
                $this->db->trans_rollback();
                return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Failed to savess");
            }

            $this->db->trans_commit();
            return set_http_response_success("Success", array(), md5($res));
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Failed to save");
        }
    }

    function perusahaan_delete($hash_id = "")
    {
        if ($this->is_allow_perusahaan_delete == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "You don't have permission to delete data");
        if (empty($hash_id)) return set_http_response_error(HTTP_BAD_REQUEST, "Failed to delete");

        // **
        // Check hash_id
        $filters = array();
        $filters["hash_id"] = $hash_id;
        $res = $this->perusahaan_engine->perusahaan_get_list($filters);
        if (count($res) == 0) return set_http_response_error(HTTP_BAD_REQUEST, "Failed to delete");
        $res = $res[0];
        $id = (int) $res["id"];

        $this->db->trans_begin();
        try {
            $res = $this->perusahaan_engine->perusahaan_delete($id);
            if ((int) $res == 0) {
                $this->db->trans_rollback();
                return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Failed to delete");
            }

            $this->db->trans_commit();
            return set_http_response_success("Success");
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Fail to delete");
        }
    }
}
