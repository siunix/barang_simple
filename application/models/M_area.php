<?php
class M_area extends MY_Model
{
    private $area_engine;

    public function __construct()
    {
        parent::__construct();

        $this->user_engine = new User_engine();
        $this->area_engine = new Area_engine();

        $session_data = $this->session->userdata(APP_SESSION_NAME);

        $this->creator_user_hash_id = $session_data["hash_id"];
        $this->creator_username = $session_data["username"];
    }

    function provinces()
    {
        $res = $this->area_engine->province_get_list();

        $final_res = array();
        foreach ($res as $index => $value) {
            $row = array();
            $row['id'] = md5($value['id']);
            $row['name'] = $value['name'];

            $final_res[] = $row;
        }

        return $final_res;
    }

    function regencies($province_hash_id)
    {
        $filters = array();
        $filters['province_hash_id'] = $province_hash_id;
        $res = $this->area_engine->regency_get_list($filters);

        $final_res = array();
        foreach ($res as $index => $value) {
            $row = array();
            $row['id'] = md5($value['id']);
            $row['name'] = $value['name'];

            $final_res[] = $row;
        }

        return $final_res;
    }

    function districts($regency_hash_id)
    {
        $filters = array();
        $filters['regency_hash_id'] = $regency_hash_id;
        $res = $this->area_engine->district_get_list($filters);

        $final_res = array();
        foreach ($res as $index => $value) {
            $row = array();
            $row['id'] = md5($value['id']);
            $row['name'] = $value['name'];

            $final_res[] = $row;
        }

        return $final_res;
    }

    function villages($district_hash_id)
    {
        $filters = array();
        $filters['district_hash_id'] = $district_hash_id;
        $res = $this->area_engine->village_get_list($filters);

        $final_res = array();
        foreach ($res as $index => $value) {
            $row = array();
            $row['id'] = md5($value['id']);
            $row['name'] = $value['name'];

            $final_res[] = $row;
        }

        return $final_res;
    }
}
