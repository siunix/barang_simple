<?php
class M_user extends MY_Model
{
    private $user_engine;
    private $creator_user_hash_id;
    private $creator_username;
    private $perusahaan_engine;
    private $user_level;

    private $is_allow_user_login;
    private $is_allow_user_login_create;
    private $is_allow_user_login_update;
    private $is_allow_user_login_delete;

    private $is_allow_user_level;
    private $is_allow_user_level_create;
    private $is_allow_user_level_update;
    private $is_allow_user_level_delete;

    public function __construct()
    {
        parent::__construct();

        $this->user_engine = new User_engine();
        $this->perusahaan_engine = new Perusahaan_engine();

        $session_data = $this->session->userdata(APP_SESSION_NAME);

        $this->is_allow_user_login = isset($session_data["allow_user_login"]) ? (int) $session_data["allow_user_login"] : 0;
        $this->is_allow_user_login_create = isset($session_data["allow_user_login_create"]) ? (int) $session_data["allow_user_login_create"] : 0;
        $this->is_allow_user_login_update = isset($session_data["allow_user_login_update"]) ? (int) $session_data["allow_user_login_update"] : 0;
        $this->is_allow_user_login_delete = isset($session_data["allow_user_login_delete"]) ? (int) $session_data["allow_user_login_delete"] : 0;

        $this->is_allow_user_level = isset($session_data["allow_user_level"]) ? (int) $session_data["allow_user_level"] : 0;
        $this->is_allow_user_level_create = isset($session_data["allow_user_level_create"]) ? (int) $session_data["allow_user_level_create"] : 0;
        $this->is_allow_user_level_update = isset($session_data["allow_user_level_update"]) ? (int) $session_data["allow_user_level_update"] : 0;
        $this->is_allow_user_level_delete = isset($session_data["allow_user_level_delete"]) ? (int) $session_data["allow_user_level_delete"] : 0;

        if ($this->is_allow_user_login == 0) {
            $this->is_allow_user_login_create = 0;
            $this->is_allow_user_login_update = 0;
            $this->is_allow_user_login_delete = 0;
        }

        if ($this->is_allow_user_level == 0) {
            $this->is_allow_user_level_create = 0;
            $this->is_allow_user_level_update = 0;
            $this->is_allow_user_level_delete = 0;
        }

        $this->creator_user_hash_id = $session_data["hash_id"];
        $this->creator_username = $session_data["username"];
        $this->user_level = $session_data["user_level"];
        $this->perusahaan_hash_id = $session_data["perusahaan_hash_id"];
    }

    // ====================================================
    // ** OBJECT USER LOGIN
    // ====================================================

    function user_login_get_list()
    {
        if ($this->is_allow_user_login == 0) return array();

        $filters = array();
        if ($this->user_level != -1) {
            $filters_perusahaan = array();
            $filters_perusahaan['hash_id'] = $this->perusahaan_hash_id;
            $res = $this->perusahaan_engine->perusahaan_get_list($filters_perusahaan);
            if (count($res) == 0) return array();
            $res = $res[0];
            $perusahaan_id = $res['id'];

            $filters['perusahaan_id'] = $perusahaan_id;
        }

        $res = $this->user_engine->user_login_get_list($filters);
        $final_res = array();
        foreach ($res as $key => $value) {
            if($value['id'] < 0) continue;

            $row = array();
            $row['id'] = md5($value['id']);
            $row['created'] = $value['created'];
            $row['name'] = ucfirst($value['name']);
            $row['username'] = ucfirst($value['username']);
            $row['user_level_name'] = $value['user_level_name'];
            $row['perusahaan_name'] = $value['perusahaan_name'];

            $final_res[] = $row;
        }

        return $final_res;
    }

    function user_login_get($hash_id = "")
    {
        if ($this->is_allow_user_login == 0) return array();
        if (empty($hash_id)) return array();

        $filters = array();
        if ($this->user_level != -1) {
            $filters_perusahaan = array();
            $filters_perusahaan['hash_id'] = $this->perusahaan_hash_id;
            $res = $this->perusahaan_engine->perusahaan_get_list($filters_perusahaan);
            if (count($res) == 0) return array();
            $res = $res[0];
            $perusahaan_id = $res['id'];

            $filters['perusahaan_id'] = $perusahaan_id;
        }

        $filters["hash_id"] = $hash_id;
        $res = $this->user_engine->user_login_get_list($filters);

        if (count($res) == 0) return array();
        $res = $res[0];

        $final_res = array();
        $final_res['id'] = md5($res['id']);
        $final_res['name'] = $res['name'];
        $final_res['username'] = $res['username'];
        $final_res['email'] = $res['email'];
        $final_res['user_level_id'] = md5($res['user_level_id']);
        $final_res['perusahaan_id'] = md5($res['perusahaan_id']);
        $final_res['is_active'] = (int) $res['is_active'];

        return $final_res;
    }

    function user_login_save()
    {
        $id = 0;

        // **
        // get user data by user hash_id
        if (!is_superadmin()) {
            $filters = array();
            $filters["hash_id"] = $this->creator_user_hash_id;
            $res = $this->user_engine->user_login_get_list($filters);
            if (count($res) == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "Invalid user");
            $res = $res[0];
            $creator_user_id = $res["id"];
            $creator_user_name = $res["username"];
        } else {
            $creator_user_id = -1;
            $creator_user_name = $this->creator_username;
        }

        // active default value
        $is_active = 0;
        $set_active_date = date("Y-m-d H:i:s");
        $set_active_user_id = $creator_user_id;
        $set_active_user_name = $creator_user_name;

        // archive default value
        $is_archive = 0;
        $set_archive_date = date("Y-m-d H:i:s");
        $set_archive_user_id = $creator_user_id;
        $set_archive_user_name = $creator_user_name;

        $hash_id = !empty($this->input->post("hash_id")) ? $this->input->post("hash_id") : "";
        $name = !empty($this->input->post("name")) ? $this->input->post("name") : "";
        $username = !empty($this->input->post("username")) ? $this->input->post("username") : "";
        $email = !empty($this->input->post("email")) ? $this->input->post("email") : "";
        $password = !empty($this->input->post("password")) ? $this->input->post("password") : "";
        $confirm_password = !empty($this->input->post("confirm_password")) ? $this->input->post("confirm_password") : "";
        $user_level_hash_id = !empty($this->input->post("user_level_hash_id")) ? $this->input->post("user_level_hash_id") : "";
        $perusahaan_hash_id = !empty($this->input->post("perusahaan_hash_id")) ? $this->input->post("perusahaan_hash_id") : "";
        $is_active = !empty($this->input->post("is_active")) ? (int) $this->input->post("is_active") : $is_active;
        $allow_approve_employee = !empty($this->input->post("allow_approve_employee")) ? (int) $this->input->post("allow_approve_employee") : 0;
        $allow_set_active_employee = !empty($this->input->post("allow_set_active_employee")) ? (int) $this->input->post("allow_set_active_employee") : 0;
        $allow_set_archive_employee = !empty($this->input->post("allow_set_archive_employee")) ? (int) $this->input->post("allow_set_archive_employee") : 0;

        if (empty($name)) return set_http_response_error(HTTP_BAD_REQUEST, "Fullname is required");
        if (empty($username)) return set_http_response_error(HTTP_BAD_REQUEST, "Username is required");
        if (empty($email)) return set_http_response_error(HTTP_BAD_REQUEST, "Email is required");
        if (empty($perusahaan_hash_id)) return set_http_response_error(HTTP_BAD_REQUEST, "Perusahaan is required");

        if (!preg_match(ALPHABET_REGEX, $name)) return set_http_response_error(HTTP_BAD_REQUEST, "Invalid name, only letters and white space allowed");
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) return set_http_response_error(HTTP_BAD_REQUEST, "Invalid email");

        if (!in_array($is_active, array(0, 1))) return set_http_response_error(HTTP_BAD_REQUEST, "invalid input");

        if (empty($hash_id)) {
            if (empty($password)) return set_http_response_error(HTTP_BAD_REQUEST, "Password is required");
            if (empty($confirm_password)) return set_http_response_error(HTTP_BAD_REQUEST, "Confirm password is required");
            if ($password != $confirm_password) return set_http_response_error(HTTP_BAD_REQUEST, "Password doesn't match");
        }

        if (empty($user_level_hash_id)) return set_http_response_error(HTTP_BAD_REQUEST, "User level is required");

        // do hash password
        $password = custom_password_encrypt($password);

        // Jika edit (hash_id != "")
        // check dan validasi id
        if (!empty($hash_id)) {
            if ($this->is_allow_user_login_update == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "you don't have permission to update data");

            $filters = array();
            $filters["hash_id"] = $hash_id;
            $res = $this->user_engine->user_login_get_list($filters);
            if (count($res) == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "User login not found");
            $res = $res[0];
            $id = $res["id"];
            $password = $res["password"];
            $is_archive = $res["is_archive"];
            $set_archive_date = $res["set_archive_date"];
            $set_archive_user_id = $res["set_archive_user_id"];
            $set_archive_user_name = $res["set_archive_user_name"];

        } else {
            if ($this->is_allow_user_login_create == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "you don't have permission to create data");

        }

        $filters_perusahaan = array();
        $filters_perusahaan['hash_id'] = $perusahaan_hash_id;
        $res = $this->perusahaan_engine->perusahaan_get_list($filters_perusahaan);
        if (count($res) == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "perusahaan tidak ditemukan");
        $res = $res[0];

        $perusahaan_id = $res['id'];
        $perusahaan_name = $res['name'];

        // check duplicate email
        $filters = array();
        $filters["email"] = $email;
        $res = $this->user_engine->user_login_get_list($filters);
        if (count($res) > 0) {
            $res = $res[0];
            $current_hash_id = md5($res["id"]);
            if ($hash_id != $current_hash_id) return set_http_response_error(HTTP_BAD_REQUEST, "duplicate user email");
        }

        // check duplicate username
        $filters = array();
        $filters["username"] = $username;
        $res = $this->user_engine->user_login_get_list($filters);
        if (count($res) > 0) {
            $res = $res[0];
            $current_hash_id = md5($res["id"]);
            if ($hash_id != $current_hash_id) return set_http_response_error(HTTP_BAD_REQUEST, "duplicate user username");
        }

        // check dan validasi user_level by user_level_hash_id
        $filters = array();
        $filters["hash_id"] = $user_level_hash_id;
        $res = $this->user_engine->user_level_get_list($filters);
        if (count($res) == 0) return set_http_response_error(HTTP_BAD_REQUEST, "User level not found");
        $res = $res[0];
        $user_level_id = $res["id"];

        $save_data = array();
        $save_data["id"] = (int) $id;
        $save_data["creator_user_id"] = !empty($creator_user_id) ? (int) $creator_user_id : 0;
        $save_data["creator_user_name"] = !empty($creator_user_name) ? trim($creator_user_name) : "";
        $save_data["name"] = !empty($name) ? trim($name) : "";
        $save_data["username"] = !empty($username) ? trim($username) : "";
        $save_data["email"] = !empty($email) ? trim($email) : "";
        $save_data["password"] = !empty($password) ? trim($password) : "";
        $save_data["user_level_id"] = !empty($user_level_id) ? (int) $user_level_id : 0;
        $save_data["is_active"] = !empty($is_active) ? (int) $is_active : 0;
        $save_data["set_active_date"] = !empty($set_active_date) ? trim($set_active_date) : null;
        $save_data["set_active_user_id"] = !empty($set_active_user_id) ? (int) $set_active_user_id : 0;
        $save_data["set_active_user_name"] = !empty($set_active_user_name) ? trim($set_active_user_name) : "";
        $save_data["is_archive"] = !empty($is_archive) ? (int) $is_archive : 0;
        $save_data["set_archive_date"] = !empty($set_archive_date) ? trim($set_archive_date) : null;
        $save_data["set_archive_user_id"] = !empty($set_archive_user_id) ? (int) $set_archive_user_id : 0;
        $save_data["set_archive_user_name"] = !empty($set_archive_user_name) ? trim($set_archive_user_name) : "";
        $save_data["perusahaan_id"] = $perusahaan_id;
        $save_data["perusahaan_name"] = $perusahaan_name;

        $this->db->trans_begin();
        try {
            $res = $this->user_engine->user_login_save($save_data);
            if ((int) $res == 0) {
                $this->db->trans_rollback();
                return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Failed to save user login");
            }

            $this->db->trans_commit();
            return set_http_response_success("Success", array(), md5($res));
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Failed to save user login");
        }
    }

    function user_login_set_active($hash_id = "")
    {
        if ($this->is_allow_user_login_update == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "you don't have permission to update data");

        if (empty($hash_id)) return set_http_response_error(HTTP_BAD_REQUEST, "Failed to activated user login");

        // **
        // Check hash_id
        $filters = array();
        $filters["hash_id"] = $hash_id;
        $res = $this->user_engine->user_login_get_list($filters);
        if (count($res) == 0) return set_http_response_error(HTTP_NOT_FOUND, "Failed to activated this user login");
        $res = $res[0];
        $id = (int) $res["id"];

        if ($id == -1) return set_http_response_error(HTTP_UNAUTHORIZED, "You don't have permission to acces action this user login");

        // **
        // get user data by user hash_id
        $filters = array();
        $filters["hash_id"] = $this->creator_user_hash_id;
        $res = $this->user_engine->user_login_get_list($filters);
        if (count($res) == 0) return set_http_response_error(HTTP_NOT_FOUND, "Invalid user");
        $res = $res[0];
        $set_active_user_id = $res["id"];
        $set_active_user_name = $res["username"];

        $save_data = array();
        $save_data["id"] = $id;
        $save_data["set_active_user_id"] = $set_active_user_id;
        $save_data["set_active_user_name"] = $set_active_user_name;

        $this->db->trans_begin();
        try {
            $res = $this->user_engine->user_login_set_active($save_data);
            if ((int) $res == 0) {
                $this->db->trans_rollback();
                return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Failed to activated this user");
            }

            $this->db->trans_commit();
            return set_http_response_success("Success");
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Fail to activated this user");
        }
    }

    function user_login_set_archive($hash_id = "")
    {
        if ($this->is_allow_user_login_update == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "you don't have permission to update data");

        if (empty($hash_id)) return set_http_response_error(HTTP_BAD_REQUEST, "Failed to archived user login");

        // **
        // Check hash_id
        $filters = array();
        $filters["hash_id"] = $hash_id;
        $res = $this->user_engine->user_login_get_list($filters);
        if (count($res) == 0) return set_http_response_error(HTTP_NOT_FOUND, "Failed to archived this user login");
        $res = $res[0];
        $id = (int) $res["id"];

        if ($id == -1) return set_http_response_error(HTTP_UNAUTHORIZED, "You don't have permission to acces action this user login");

        // **
        // get user data by user hash_id
        $filters = array();
        $filters["hash_id"] = $this->creator_user_hash_id;
        $res = $this->user_engine->user_login_get_list($filters);
        if (count($res) == 0) return set_http_response_error(HTTP_NOT_FOUND, "Invalid user");
        $res = $res[0];
        $set_archive_user_id = $res["id"];
        $set_archive_user_name = $res["username"];

        $save_data = array();
        $save_data["id"] = $id;
        $save_data["set_archive_user_id"] = $set_archive_user_id;
        $save_data["set_archive_user_name"] = $set_archive_user_name;

        $this->db->trans_begin();
        try {
            $res = $this->user_engine->user_login_set_archive($save_data);
            if ((int) $res == 0) {
                $this->db->trans_rollback();
                return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Failed to archived this user");
            }

            $this->db->trans_commit();
            return set_http_response_success("Success");
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Fail to archived this user");
        }
    }

    // **
    // This is optional function, be carefull to use this function
    function user_login_delete($hash_id = "")
    {
        if ($this->is_allow_user_login_delete == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "you don't have permission to delete data");

        if (empty($hash_id)) return set_http_response_error(HTTP_BAD_REQUEST, "Failed to delete user login");

        // **
        // Check hash_id
        $filters = array();
        $filters["hash_id"] = $hash_id;
        $res = $this->user_engine->user_login_get_list($filters);
        if (count($res) == 0) return set_http_response_error(HTTP_NOT_FOUND, "Failed to delete user login");
        $res = $res[0];
        $id = (int) $res["id"];
        $user_level_id = (int) $res["user_level_id"];

        if ($id == -1) return set_http_response_error(HTTP_NOT_FOUND, "Failed to delete this user");
        if ($user_level_id == -1) return set_http_response_error(HTTP_BAD_REQUEST, "Failed to delete this user");

        $this->db->trans_begin();
        try {
            $res = $this->user_engine->user_login_delete($id);
            if ((int) $res == 0) {
                $this->db->trans_rollback();
                return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Failed to delete this user");
            }

            $this->db->trans_commit();
            return set_http_response_success("Success");
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Fail to delete this user");
        }
    }

    function do_reset_password()
    {
        $user_login_hash_id = $this->input->post("hash_id");
        $old_password = $this->input->post("old_password");
        $new_password = $this->input->post("new_password");
        $confirm_password = $this->input->post("confirm_password");

        if (empty($user_login_hash_id)) return set_http_response_error(HTTP_BAD_REQUEST, "Invalid user login");
        if (empty($old_password)) return set_http_response_error(HTTP_BAD_REQUEST, "Invalid old password");
        if (empty($new_password)) return set_http_response_error(HTTP_BAD_REQUEST, "Invalid new password");
        if (empty($confirm_password)) return set_http_response_error(HTTP_BAD_REQUEST, "Invalid confirm password");

        if ($new_password != $confirm_password) return set_http_response_error(HTTP_BAD_REQUEST, "Password confirmation doesn't match");

        // **
        // get user data by user hash_id
        if (!is_superadmin()) {
            // **
            // get user login data by user_login_hash_id
            $filters = array();
            $filters["hash_id"] = $user_login_hash_id;
            $res = $this->user_engine->user_login_get_list($filters);
            if (count($res) == 0) return set_http_response_error(HTTP_NOT_FOUND, "Invalid user login");
            $res = $res[0];
            $id = $res["id"];
            $saved_old_password = $res["password"];
        } else {
            $login_data = array(
                "md5(id)" => $user_login_hash_id,
            );

            $res = $this->db->get_where("user_login", $login_data)->result_array();
            if (count($res) == 0) return set_http_response_error(HTTP_NOT_FOUND, "Invalid user login");
            $res = $res[0];
            $id = $res["id"];
            $saved_old_password = $res["password"];
        }

        if ($saved_old_password != custom_password_encrypt($old_password)) return set_http_response_error(HTTP_BAD_REQUEST, "Wrong old password");

        $new_password = custom_password_encrypt($new_password);

        $save_data = array();
        $save_data["id"] = $id;
        $save_data["new_password"] = $new_password;

        $this->db->trans_begin();
        try {
            $res = $this->user_engine->user_login_set_password($save_data);
            if ((int) $res == 0) {
                $this->db->trans_rollback();
                return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Failed to update password");
            }

            $this->db->trans_commit();
            return set_http_response_success("Success, Login again!");
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Fail to update password");
        }
    }

    // ====================================================
    // ** END of OBJECT USER LOGIN
    // ====================================================

    // ====================================================
    // ** OBJECT USER LEVEL
    // ====================================================

    function user_level_get_list()
    {
        if ($this->is_allow_user_login == 0) return array();

        $filters = array();
        $res = $this->user_engine->user_level_get_list($filters);

        $final_res = array();
        foreach ($res as $key => $value) {
            if($value['id'] < 0) continue;

            $row = array();
            $row['id'] = md5($value['id']);
            $row['created'] = $value['created'];
            $row['name'] = $value['name'];

            $final_res[] = $row;
        }

        return $final_res;
    }

    function user_level_get($hash_id = "")
    {
        if ($this->is_allow_user_login == 0) return array();

        if (empty($hash_id)) return array();

        $filters = array();
        $filters["hash_id"] = $hash_id;
        $res = $this->user_engine->user_level_get_list($filters);

        if (count($res) == 0) return array();
        $res = $res[0];

        return $res;
    }

    function user_level_save()
    {
        $id = 0;
        $hash_id = $this->input->post("hash_id");
        $name = $this->input->post("name");

        // Jika edit (hash_id != "")
        // check dan validasi id
        if (!empty($hash_id)) {
            if ($this->is_allow_user_level_update == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "You don't have permission to update data");

            $filters = array();
            $filters["hash_id"] = $hash_id;
            $res = $this->user_engine->user_level_get_list($filters);
            if (count($res) == 0) return set_http_response_error(HTTP_BAD_REQUEST, "User level not found");
            $res = $res[0];
            $id = $res["id"];
        } else {
            if ($this->is_allow_user_level_create == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "you don't have permission to create data");
        }

        if (empty($name)) return set_http_response_error(HTTP_BAD_REQUEST, "User level name is required");

        // **
        // get user data by user hash_id
        if (!is_superadmin()) {
            $filters = array();
            $filters["hash_id"] = $this->creator_user_hash_id;
            $res = $this->user_engine->user_login_get_list($filters);
            if (count($res) == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "You don't have permission to access this module");
            $res = $res[0];
            $creator_user_id = $res["id"];
            $creator_user_name = $res["username"];
        } else {
            $creator_user_id = -1;
            $creator_user_name = $this->creator_username;
        }

        // **
        // Set user allowed action for user_login
        $allow_user_login = !empty($this->input->post("allow_user_login")) ? (int) $this->input->post("allow_user_login") : 0;
        $allow_user_login_create = !empty($this->input->post("allow_user_login_create")) ? (int) $this->input->post("allow_user_login_create") : 0;
        $allow_user_login_update = !empty($this->input->post("allow_user_login_update")) ? (int) $this->input->post("allow_user_login_update") : 0;
        $allow_user_login_delete = !empty($this->input->post("allow_user_login_delete")) ? (int) $this->input->post("allow_user_login_delete") : 0;
        // -- end user_login

        // **
        // Set user allowed action for user_level
        $allow_user_level = !empty($this->input->post("allow_user_level")) ? (int) $this->input->post("allow_user_level") : 0;
        $allow_user_level_create = !empty($this->input->post("allow_user_level_create")) ? (int) $this->input->post("allow_user_level_create") : 0;
        $allow_user_level_update = !empty($this->input->post("allow_user_level_update")) ? (int) $this->input->post("allow_user_level_update") : 0;
        $allow_user_level_delete = !empty($this->input->post("allow_user_level_delete")) ? (int) $this->input->post("allow_user_level_delete") : 0;
        // -- end user_level

        // **
        // Set user allowed action for perusahaan
        $allow_perusahaan = !empty($this->input->post("allow_perusahaan")) ? (int) $this->input->post("allow_perusahaan") : 0;
        $allow_perusahaan_create = !empty($this->input->post("allow_perusahaan_create")) ? (int) $this->input->post("allow_perusahaan_create") : 0;
        $allow_perusahaan_update = !empty($this->input->post("allow_perusahaan_update")) ? (int) $this->input->post("allow_perusahaan_update") : 0;
        $allow_perusahaan_delete = !empty($this->input->post("allow_perusahaan_delete")) ? (int) $this->input->post("allow_perusahaan_delete") : 0;
        // -- end perusahaan

        // **
        // Set user allowed action for barang
        $allow_barang = !empty($this->input->post("allow_barang")) ? (int) $this->input->post("allow_barang") : 0;
        $allow_barang_create = !empty($this->input->post("allow_barang_create")) ? (int) $this->input->post("allow_barang_create") : 0;
        $allow_barang_update = !empty($this->input->post("allow_barang_update")) ? (int) $this->input->post("allow_barang_update") : 0;
        $allow_barang_delete = !empty($this->input->post("allow_barang_delete")) ? (int) $this->input->post("allow_barang_delete") : 0;
        // -- end barang

        $allow_item_list = array();

        $allow_item_list["allow_user_login"] = $allow_user_login;
        $allow_item_list["allow_user_login_create"] = $allow_user_login_create;
        $allow_item_list["allow_user_login_update"] = $allow_user_login_update;
        $allow_item_list["allow_user_login_delete"] = $allow_user_login_delete;

        $allow_item_list["allow_user_level"] = $allow_user_level;
        $allow_item_list["allow_user_level_create"] = $allow_user_level_create;
        $allow_item_list["allow_user_level_update"] = $allow_user_level_update;
        $allow_item_list["allow_user_level_delete"] = $allow_user_level_delete;

        $allow_item_list["allow_perusahaan"] = $allow_perusahaan;
        $allow_item_list["allow_perusahaan_create"] = $allow_perusahaan_create;
        $allow_item_list["allow_perusahaan_update"] = $allow_perusahaan_update;
        $allow_item_list["allow_perusahaan_delete"] = $allow_perusahaan_delete;

        $allow_item_list["allow_barang"] = $allow_barang;
        $allow_item_list["allow_barang_create"] = $allow_barang_create;
        $allow_item_list["allow_barang_update"] = $allow_barang_update;
        $allow_item_list["allow_barang_delete"] = $allow_barang_delete;

        $allowed_json = json_encode($allow_item_list);

        $save_data = array();
        $save_data["id"] = (int) $id;
        $save_data["creator_user_id"] = !empty($creator_user_id) ? (int) $creator_user_id : 0;
        $save_data["creator_user_name"] = !empty($creator_user_name) ? trim($creator_user_name) : "";
        $save_data["name"] = !empty($name) ? trim($name) : "";
        $save_data["allowed_json"] = !empty($allowed_json) ? trim($allowed_json) : "[]";

        $this->db->trans_begin();
        try {
            $res = $this->user_engine->user_level_save($save_data);
            if ((int) $res == 0) {
                $this->db->trans_rollback();
                return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Failed to save user level");
            }

            $this->db->trans_commit();
            return set_http_response_success("Success", array(), md5($res));
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Failed to save user level");
        }
    }

    function user_level_delete($hash_id = "")
    {
        if ($this->is_allow_user_login_delete == 0) return set_http_response_error(HTTP_UNAUTHORIZED, "You don't have permission to delete data");
        if (empty($hash_id)) return set_http_response_error(HTTP_BAD_REQUEST, "Failed to delete user level");

        if (md5($this->user_level) == $hash_id) return set_http_response_error(HTTP_BAD_REQUEST, "You cant delete your active level access");

        // **
        // Check hash_id
        $filters = array();
        $filters["hash_id"] = $hash_id;
        $res = $this->user_engine->user_level_get_list($filters);
        if (count($res) == 0) return set_http_response_error(HTTP_BAD_REQUEST, "Failed to delete user level");
        $res = $res[0];
        $id = (int) $res["id"];

        if ($id < 0) return set_http_response_error(HTTP_UNAUTHORIZED, "You don't have permission to delete data");

        // **
        // check apakah level digunakan
        $filters = array();
        $filters["user_level_id"] = $id;
        $res = $this->user_engine->user_login_get_list($filters);
        if (count($res) > 0) return set_http_response_error(HTTP_BAD_REQUEST, "can't to delete this level, User level has been used");

        $this->db->trans_begin();
        try {
            $res = $this->user_engine->user_level_delete($id);
            if ((int) $res == 0) {
                $this->db->trans_rollback();
                return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Failed to delete this level");
            }

            $this->db->trans_commit();
            return set_http_response_success("Success");
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return set_http_response_error(HTTP_INTERNAL_SERVER_ERROR, "Fail to delete this level");
        }
    }

    // ====================================================
    // ** END of OBJECT USER LEVEL
    // ====================================================
}
