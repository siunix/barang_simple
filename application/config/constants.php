<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') or define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  or define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') or define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   or define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  or define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           or define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     or define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       or define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  or define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   or define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              or define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            or define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       or define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        or define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          or define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         or define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   or define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  or define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') or define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     or define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       or define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      or define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      or define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

if (!defined('MAX_PHOTO_SIZE')) define('MAX_PHOTO_SIZE', 1500000);
if (!defined('UPLOAD_IMAGE_TYPE_ALLOWED')) define('UPLOAD_IMAGE_TYPE_ALLOWED', array("png", "jpg", "jpeg"));

if (!defined('APP_NAME')) define('APP_NAME', APPLICATION_NAME);
if (!defined('APP_INIT_NAME')) define('APP_INIT_NAME', APPLICATION_INIT_NAME);
if (!defined('APP_DESCRIPTION')) define('APP_DESCRIPTION', "Barang Sederhana");
if (!defined('APP_PROGRAMMER')) define('APP_PROGRAMMER', "abd.siunix@gmail.com");

if (!defined('APP_SESSION_NAME')) define('APP_SESSION_NAME', SESSION_NAME);

// ** REGEX VARIABLE
if (!defined('ALPHABET_REGEX')) define('ALPHABET_REGEX', "/^[a-zA-Z.,& ]*$/");
if (!defined('NUMERIC_REGEX')) define('NUMERIC_REGEX', "/^[0-9]*$/");

// Informational
if (!defined("HTTP_CONTINUE")) define("HTTP_CONTINUE", 100);
if (!defined("HTTP_SWITCHING_PROTOCOLS")) define("HTTP_SWITCHING_PROTOCOLS", 101);
if (!defined("HTTP_PROCESSING")) define("HTTP_PROCESSING", 102);

// Success
if (!defined("HTTP_OK")) define("HTTP_OK", 200);
if (!defined("HTTP_CREATED")) define("HTTP_CREATED", 201);
if (!defined("HTTP_ACCEPTED")) define("HTTP_ACCEPTED", 202);
if (!defined("HTTP_NON_AUTHORITATIVE_INFORMATION")) define("HTTP_NON_AUTHORITATIVE_INFORMATION", 203);
if (!defined("HTTP_NO_CONTENT")) define("HTTP_NO_CONTENT", 204);
if (!defined("HTTP_RESET_CONTENT")) define("HTTP_RESET_CONTENT", 205);
if (!defined("HTTP_PARTIAL_CONTENT")) define("HTTP_PARTIAL_CONTENT", 206);
if (!defined("HTTP_MULTI_STATUS")) define("HTTP_MULTI_STATUS", 207);
if (!defined("HTTP_ALREADY_REPORTED")) define("HTTP_ALREADY_REPORTED", 208);
if (!defined("HTTP_IM_USED")) define("HTTP_IM_USED", 226);

// Redirection
if (!defined("HTTP_MULTIPLE_CHOICES")) define("HTTP_MULTIPLE_CHOICES", 300);
if (!defined("HTTP_MOVED_PERMANENTLY")) define("HTTP_MOVED_PERMANENTLY", 301);
if (!defined("HTTP_FOUND")) define("HTTP_FOUND", 302);
if (!defined("HTTP_SEE_OTHER")) define("HTTP_SEE_OTHER", 303);
if (!defined("HTTP_NOT_MODIFIED")) define("HTTP_NOT_MODIFIED", 304);
if (!defined("HTTP_USE_PROXY")) define("HTTP_USE_PROXY", 305);
if (!defined("HTTP_RESERVED")) define("HTTP_RESERVED", 306);
if (!defined("HTTP_TEMPORARY_REDIRECT")) define("HTTP_TEMPORARY_REDIRECT", 307);
if (!defined("HTTP_PERMANENTLY_REDIRECT")) define("HTTP_PERMANENTLY_REDIRECT", 308);

// Client Error
if (!defined("HTTP_BAD_REQUEST")) define("HTTP_BAD_REQUEST", 400);
if (!defined("HTTP_UNAUTHORIZED")) define("HTTP_UNAUTHORIZED", 401);
if (!defined("HTTP_PAYMENT_REQUIRED")) define("HTTP_PAYMENT_REQUIRED", 402);
if (!defined("HTTP_FORBIDDEN")) define("HTTP_FORBIDDEN", 403);
if (!defined("HTTP_NOT_FOUND")) define("HTTP_NOT_FOUND", 404);
if (!defined("HTTP_METHOD_NOT_ALLOWED")) define("HTTP_METHOD_NOT_ALLOWED", 405);
if (!defined("HTTP_NOT_ACCEPTABLE")) define("HTTP_NOT_ACCEPTABLE", 406);
if (!defined("HTTP_PROXY_AUTHENTICATION_REQUIRED")) define("HTTP_PROXY_AUTHENTICATION_REQUIRED", 407);
if (!defined("HTTP_REQUEST_TIMEOUT")) define("HTTP_REQUEST_TIMEOUT", 408);
if (!defined("HTTP_CONFLICT")) define("HTTP_CONFLICT", 409);
if (!defined("HTTP_GONE")) define("HTTP_GONE", 410);
if (!defined("HTTP_LENGTH_REQUIRED")) define("HTTP_LENGTH_REQUIRED", 411);
if (!defined("HTTP_PRECONDITION_FAILED")) define("HTTP_PRECONDITION_FAILED", 412);
if (!defined("HTTP_REQUEST_ENTITY_TOO_LARGE")) define("HTTP_REQUEST_ENTITY_TOO_LARGE", 413);
if (!defined("HTTP_REQUEST_URI_TOO_LONG")) define("HTTP_REQUEST_URI_TOO_LONG", 414);
if (!defined("HTTP_UNSUPPORTED_MEDIA_TYPE")) define("HTTP_UNSUPPORTED_MEDIA_TYPE", 415);
if (!defined("HTTP_REQUESTED_RANGE_NOT_SATISFIABLE")) define("HTTP_REQUESTED_RANGE_NOT_SATISFIABLE", 416);
if (!defined("HTTP_EXPECTATION_FAILED")) define("HTTP_EXPECTATION_FAILED", 417);
if (!defined("HTTP_I_AM_A_TEAPOT")) define("HTTP_I_AM_A_TEAPOT", 418);
if (!defined("HTTP_UNPROCESSABLE_ENTITY")) define("HTTP_UNPROCESSABLE_ENTITY", 422);
if (!defined("HTTP_LOCKED")) define("HTTP_LOCKED", 423);
if (!defined("HTTP_FAILED_DEPENDENCY")) define("HTTP_FAILED_DEPENDENCY", 424);
if (!defined("HTTP_RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL")) define("HTTP_RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL", 425);
if (!defined("HTTP_UPGRADE_REQUIRED")) define("HTTP_UPGRADE_REQUIRED", 426);
if (!defined("HTTP_PRECONDITION_REQUIRED")) define("HTTP_PRECONDITION_REQUIRED", 428);
if (!defined("HTTP_TOO_MANY_REQUESTS")) define("HTTP_TOO_MANY_REQUESTS", 429);
if (!defined("HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE")) define("HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE", 431);

// Server Error
if (!defined("HTTP_INTERNAL_SERVER_ERROR")) define("HTTP_INTERNAL_SERVER_ERROR", 500);
if (!defined("HTTP_NOT_IMPLEMENTED")) define("HTTP_NOT_IMPLEMENTED", 501);
if (!defined("HTTP_BAD_GATEWAY")) define("HTTP_BAD_GATEWAY", 502);
if (!defined("HTTP_SERVICE_UNAVAILABLE")) define("HTTP_SERVICE_UNAVAILABLE", 503);
if (!defined("HTTP_GATEWAY_TIMEOUT")) define("HTTP_GATEWAY_TIMEOUT", 504);
if (!defined("HTTP_VERSION_NOT_SUPPORTED")) define("HTTP_VERSION_NOT_SUPPORTED", 505);
if (!defined("HTTP_VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL")) define("HTTP_VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL", 506);
if (!defined("HTTP_INSUFFICIENT_STORAGE")) define("HTTP_INSUFFICIENT_STORAGE", 507);
if (!defined("HTTP_LOOP_DETECTED")) define("HTTP_LOOP_DETECTED", 508);
if (!defined("HTTP_NOT_EXTENDED")) define("HTTP_NOT_EXTENDED", 510);
if (!defined("HTTP_NETWORK_AUTHENTICATION_REQUIRED")) define("HTTP_NETWORK_AUTHENTICATION_REQUIRED", 511);

// Token Attributes
if (!defined('TOKEN_LENGTH')) define('TOKEN_LENGTH', 6);
if (!defined('TOKEN_CHARACTER')) define('TOKEN_CHARACTER', "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");