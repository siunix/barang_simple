<?php
define("BASE_URL", "http://127.0.0.1/barang_simple/");

define("DB_HOST", "localhost");
define("DB_NAME", "barang_simple");
define("DB_USER", "root");
define("DB_PASS", "12345678");

define("SESSION_NAME", "_WADAS_");
define("APPLICATION_NAME", "STACK");
define("APPLICATION_INIT_NAME", "ST");

define("LIBRARIES_LIST", array(
  'database',
  'session',
  'email',
  'db_engine',
  'user_engine',
  'perusahaan_engine',
  'area_engine',
  'barang_engine',
));

?>